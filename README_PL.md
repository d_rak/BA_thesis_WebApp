# Aplikacja internetowa obsługująca proces rejestracji i rezerwacji tematów prac dyplomowych - Praca licencjacka 2019


## ℹ️ README ENG
[README in English language](README.md)

## 🖥 Wizualizacja (zrzut ekranu)
<img src="screenshot/praca_licencjacka_portfolio.webp" width="850">

## 📝 Opis
Celem projektu było stworzenie od podstaw aplikacji umożliwiającej rezerwację tematów prac licencjackich i magisterskich przez studentów. Uwzględniłem możliwość dodawania nowych tematów przez promotorów poprzez przygotowany szablon pliku. Aplikacja miała zawierać, również panel administracyjny dla pracowników sekretariatu.

Prezentowany temat został przeze mnie zaproponowany podczas zauważonych trudności studentów przy dokonywaniu rezerwacji dostępnych tematów prac licencjackich. Chcąc usprawnić ten proces postanowiłem przygotować webową aplikację.
 
<br>
<br>

Więcej informacji o projekcie znajduje się pod linkiem  [itsparta.pl/CV/DariuszRak/#BA_thesis_app](https://itsparta.pl/CV/DariuszRak/#BA_thesis_app)

Zapraszam do odwiedzenia strony. 🙂 

## 🎯 Funkcjonalności

W aplikacji zostały wdrożone główne funkcje:
- dodawanie tematów prac dyplomowych na podstawie przygotowanego szablonu pliku .docx
- rezerwacja lub potwierdzenie przydzielenia tematu pracy dyplomowej przez Studenta (wstępna rezerwacja)
- generowanie deklaracji wyboru tematu pracy dyplomowej (plik PDF)
- wysyłanie wiadomości email (potwierdzenie wstępnej rezerwacji tematu)
- panel administracyjny dla sekretariatu (weryfikacja i końcowe zatwierdzenie wyboru tematu pracy dyplomowej)
- statystyki wolnych i zajętych tematów (w panelu administracyjnym)
- usuwanie tematów prac dyplomowych (opcja w panelu administracyjnym)

## 💡 Użyte technologie \ biblioteki \ narzędzia \ założenia
![PHP](https://img.shields.io/badge/-PHP7-787cb4) 
![JavaScript](https://img.shields.io/badge/-JavaScript-edd619) 
![MariaDB](https://img.shields.io/badge/-MariaDB-b76f54) 
![HTML5](https://img.shields.io/badge/-HTML5-e54e26) 
![CSS3](https://img.shields.io/badge/-CSS3-1472b7) 
![Bootstrap](https://img.shields.io/badge/-Bootstrap4-703cbe) 
![Git](https://img.shields.io/badge/-Git-f05033) 

![mPDF](https://img.shields.io/badge/-mPDF-7d7d7d) 
![PHPMailer](https://img.shields.io/badge/-PHPMailer-7d7d7d) 
![PHPWord](https://img.shields.io/badge/-PHPWord-7d7d7d) 

![WebApp](https://img.shields.io/badge/-WebApp-lightgrey) 
![MVC](https://img.shields.io/badge/-MVC-lightgrey) 

## 📄 Uruchomienie \ przetestowanie projektu

Projekt można pobrać i uruchomić przy pomocy docker'a
```sh
$ cd .\.docker\
$ docker-compose build
$ docker-compose up
```


## 📄 Uwagi
Wiem, że w tym projekcie można wiele poprawić pod względem: graficznym, UI/UX, bezpieczeństwa i samego kodu. Był to projekt edukacyjny i obecnie nie rozwijany. Publikuję go, aby pokazać swoją początkową ścieżkę rozwoju oraz postęp stosunku do kolejnych zbudowanych projektów w przyszłości.

Przy okazji drogi czytelniku, mały powrót do przeszłości 🙂 Przypomnij sobie własną pracę naukową: licencjacką lub magisterską. Czy Ty, również ją pozytywnie (z łezką w oku) wspominasz 😉?

## 🙋‍♂️ Kontakt
Kontakt  na stronie internetowej [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

## 📄 Licencja
Zezwalam na pobranie i przetestowanie kodu. Zaznaczam jednak, że kod uruchomiany / użytkowany jest na własną odpowiedzialność.

## 🏁 Status projektu
Projekt został stworzony do celów edukacyjnych w roku 2019. Obecnie nie jest rozwijany. 

