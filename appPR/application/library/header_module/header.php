<?php

/**
 * ModuleName: Nagłowek
 */

$url=SERVER_ADDRESS."search";
echo '
    <nav class="navbar navbar-dark bg-dark justify-content-between  pb-0">
        <a class="navbar-brand text-left" href="">System rejestracji i&nbsp;rezerwacji<br /> tematów prac dyplomowych </a>
        <form class="" action="'.$url.'" method="GET">
            <div class="row float-right px-3" >
                <div class="col-12 col-sm-8 m-0 p-0 align-self-center">
                    <input id="search_input" class="form-control mr-sm-3 col-2" type="search" placeholder="Szukaj temat" aria-label="Search" required name="search">
                </div>
                <div class="col-12 col-sm-4 text-left m-0 p-0">
                    <button class="btn btn-secondary small col-3 my-2 my-sm-0" type="submit">Szukaj</button>
                </div> 
            </div> 
        </form>
    </nav>
'; 



//do poprawy
?>