<?php

/**
 * ModuleName: Administracja menu 
 */

$url=SERVER_ADDRESS."administrator";
echo '
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark p-3 ">
                <button class="navbar-toggler mb-2 " type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                    MENU
                </button>
                <div class="collapse navbar-collapse" id="navbarColor02">
                    <ul class="navbar-nav mr-auto text-left">
                        <li class="nav-item">
                            <a class="nav-link  pl-0" href="'.$url.'">STATYSTYKI</a>
                        </li>  
                        <li class="nav-item">
                            <a class="nav-link  pl-0" href="'.$url.'/show/ba">Tematy prac LICENCJACKICH</a>
                        </li>                  
                        <li class="nav-item">
                            <a class="nav-link" href="'.$url.'/show/ma">Tematy prac Magisterskich</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="'.$url.'/year">Rok akademicki</a>
                        </li>
                    </ul> 
                </div>
                <a class="nav-link btn-warning text-center " href="administrator/logout">Wyloguj <span class="text-primary">'.model_load("administratormodel", "getLoggedUser", "").'</span> </a> 
            </nav>';
?>
