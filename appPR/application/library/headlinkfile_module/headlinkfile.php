<?php

/**
 * ModuleName: naglowek linkowanie plików
 */

echo '
    <link rel="stylesheet" type="text/css" href="'.directory_stylesheet().'style.css" />
    <script src="'.directory_javascript().'jquery-3.4.1.slim.min.js"></script>
    <script src="'.directory_javascript().'bootstrap.min.js"></script>
    <link href="'.directory_images().'favicon.ico" rel="icon" type="image/x-icon" /> 
    ';

?>