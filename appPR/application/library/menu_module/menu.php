<?php

/**
 * ModuleName: Menu główne
 */

echo '
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark p-3">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            MENU
        </button>
        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link pl-0" href="thesis/view/ba">Tematy prac LICENCJACKICH <span class="sr-only">(current)</span></a>
                </li>                  
                    <li class="nav-item">
                    <a class="nav-link" href="thesis/view/ma">Tematy prac Magisterskich</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="info">Informacje</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="faq">FAQ</a>
                </li>
            </ul>  
        </div>
    </nav>
    ';

?>