<?php

class thesis extends controller
{
	public function __call($method, $args){
		if(!is_callable($method))
		{
			$this->sgException->errorPage(404);
		}
	}
	
	public function main() { }
	
	public function index(){ 
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function view(){ 
		$this->model->view;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function res(){ 
		$this->model->res;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function edit(){ 
		$this->model->edit;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function save(){ 
		$this->model->save;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function pdf(){ 
		$this->model->pdf;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
		
	}
	public function add(){ 
		$this->model->add;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
	public function new(){ 	
		$this->model->new;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
	public function del_res(){ 
		$this->model->del_res;
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
}
?>