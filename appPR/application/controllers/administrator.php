<?php

class administrator extends controller
{
	public function __call($method, $args)
	{
		if(!is_callable($method))
		{
			$this->sgException->errorPage(404);
		}
	}
	
	public function main() { }
	
	public function index(){
		$this->model->administrator;
		
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
	
	public function year(){
		$this->model->administrator;
		$this->model->year;
		 	
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function dashboard(){
		$this->model->administrator;
		$this->model->dashboard;	

		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
	
	public function show(){ 
		$this->model->administrator;
		$this->model->show;

		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function accept(){ 
		$this->model->administrator;
		$this->model->accept;

		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function del(){ 
		$this->model->administrator;
		$this->model->del_thesis;

		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
	public function search(){ 
		$this->model->administrator;
		$this->model->searchadmin;

		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}
		
	public function logout(){
		$this->model->administrator->logout();
	}
}
?>