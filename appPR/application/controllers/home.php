<?php

class home extends controller
{
	public function __call($method, $args)
	{
		if(!is_callable($method))
		{
			$this->sgException->errorPage(404);
		}
	}
	
	public function main() { }
	
	public function index(){	
		$this->main->module_helper;
		$this->main->model_helper;
		$this->main->directory_helper;
	}

	public function logout()
	{
		$this->model->thesis->logout();
	}
}

?>