<?php

class viewmodel extends generalmodel
{
	
	public function getTopicThesis($idThesis){ 
		$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,scientific_aspect,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status FROM thesis
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE thesis.id='".$idThesis."' AND visible=1 LIMIT 1")[0];
		if($thesis==NULL){
			$this->goToErorrPage();
		}
		$typeThesis='';
		if(isset($this->__params[0]) && !empty($this->__params[0]))
		{
			$typeThesis=$this->getTypeThesis(2);
		}	

		echo '<h3 class="text-dark m-2 mb-sm-4 text-center text-uppercase">Rok akademicki '.$thesis['years'].'</h3>';
		echo '
		<div class="table-responsive ">
			<table class="table-light text-dark p-0 m-0">
				<tr class="table-primary ">
					<td class="col-1 col-sm-3 p-md-4">Promotor:</td>
					<td class="col-11 col-sm-9 p-md-4">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Temat pracy dyplomowej <br /> (j. polski):</td>
					<td class="p-md-4"  style="min-width:300px !important">'.$thesis['topic'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Temat pracy dyplomowej <br /> (j. angielski):</td>
					<td class="p-md-4">'.$thesis['topic_in_english'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Zakres pracy i oczekiwane rezultaty praktyczne:</td>
					<td class="p-md-4">'.$thesis['scope_of_work'].'</td>
				</tr>';

				if($typeThesis=="ma") {
					echo '		
					<tr>
						<td class="p-md-4">Aspekt naukowy, problemowy, innowacyjny pracy:</td>
						<td class="p-md-4">'.$thesis['scientific_aspect'].'</td>
					</tr>';
				}
				echo '		
				<tr>
					<td class="p-md-4">Wymagane oprogramowanie / języki programowania:</td>
					<td class="p-md-4">'.$thesis['required_software'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Środowisko uruchomieniowe:</td>
					<td class="p-md-4">'.$thesis['runtime'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Dodatkowe wymagania i uwagi:</td>
					<td class="p-md-4">'.$thesis['additional_requirements'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Literatura:</td>
					<td class="p-md-4">'.$thesis['literature'].'</td>
				</tr>
				<tr>
					<td class="p-md-4">Status tematu:</td>
					<td class="text-uppercase p-md-4">'.$thesis['status'].'</td>
				</tr>
			</table>
		</div>
		<div class="row pt-sm-3">
			<div class="col-6 float-left">';
				$this->getReturnButton("WRÓĆ DO TEMATÓW");
			echo'</div>
			<div class="col-6 float-right">';		
				if($thesis['status']=='dostępny'){	
					$type_button='success';
					echo'<a href="thesis/res/'.$typeThesis.'/'.$thesis['id'].'" class="btn btn-'.$type_button.' float-right p-2 mt-2 mb-2" role="button" >REZERWUJ</a>';		
				}
		echo'</div>
		</div>
		';
	}

	public function getThesisData(){
		$shortTypeThesis=$this->getTypeThesis(2);
		if(isset($this->__params[1]) && !empty($this->__params[1])){
			$idThesis=htmlentities($this->__params[1], ENT_QUOTES);
			$this->getTopicThesis($idThesis);
		}
		else{
			$this->getThesesList();
		}
	}

	public function getThesesList(){
		$shortTypeThesis=$this->getTypeThesis(2);
		$typeThesis=$this->getTypeThesis(0);
		$years=$this->getYearsThesis($typeThesis);
		foreach($years as $year){
			echo '<h3 class="text-dark  m-2  text-center text-uppercase">Rok akademicki '.$year['years'].' <br /> Tematy prac '.$this->getTypeThesis(1).'</h3>'; 
			$theses =  $this->getTheses($year['id'], $typeThesis);     
			$promoter='';
			foreach($theses as $thesis)
			{
				if($promoter!=$thesis['id_promoter']){
					$promoter=$thesis['id_promoter'];
					echo '
					<div class="row table-primary text-center m-0">
						<div class="col ">
							<p class="m-sm-4 m-2">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</p>
						</div>
					</div>';
				}
				echo'
				<div class="row table-light m-0 p-0 border border-top-0">
					<div class="col-md-9 p-md-4 align-self-center text-justify">
						<a href="thesis/view/'.$shortTypeThesis.'/'.$thesis['id'].'" >'.$thesis['topic'].'</a>
					</div>
					<div class="col-md-3 text-center p-0 m-0 align-self-center">';
						if($thesis['status']=='dostępny'){
							$type_button='success';
							$status_button='';
						}
						else if($thesis['status']=='zarezerwowany'){
							$type_button='danger';
							$status_button='disabled';
						}
						else{
							$type_button='warning';
							$status_button='disabled';
						} 
						echo'<a href="thesis/res/'.$shortTypeThesis.'/'.$thesis['id'].'" class="btn btn-'.$type_button.' '.$status_button.' p-2 mt-2 mb-2" role="button" >'. $thesis['status'].'</a>
					</div> 
				</div>';
			}
		}
	}
}

?>