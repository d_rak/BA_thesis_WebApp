<?php

class newmodel extends generalmodel
{
	public function sendEmail($email,$addresWebThesis,$access_code){
		$e = registry::register("mailer");
		$wyslane=$e->MailGenerator($email,$addresWebThesis,$access_code); 
		return $wyslane;
	}

	public function drawNewThesis(){
		$now = time();
		$expiryTime = 1800;
		$idYear="";
		if(isset($this->__params[0]) && !empty($this->__params[0]))
		{
			$idYear=htmlentities($this->__params[0], ENT_QUOTES);
		}
		if (!isset($_SESSION['idYear'])){
				$idYear=htmlentities($this->__params[0], ENT_QUOTES);
				header("Location: ".SERVER_ADDRESS."thesis/add/".$idYear);
		}
		else if ((int)$_SESSION['lastTrace'] + $expiryTime < $now){
			$sessionName = session_name();    
			$_SESSION = array();
			if (isset($_COOKIE[$sessionName]))
			{
				setcookie($sessionName, '', $now-3600, '/');
			}
			session_destroy();
			header("Location:");
		}
		else{
			if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']){
				session_destroy();
				header("Location:");
			}
			else if($_SESSION['idYear'] != $idYear){
				$this->getFormLoginAddThesis();
			}
			else{
				$year = $this->__db->execute("SELECT *
				FROM year
				WHERE id='".$idYear."' LIMIT 1");
				if($year==Null || $idYear==Null){
					header("Location: ".SERVER_ADDRESS."thesis/add/".$idYear);
				}
				else{
					$year=$year[0];
				}

				if(isset($this->__params['POST']['thesis'])){
					$this->AddThesis($year);
				}
				else{
					$this->getTableThesis($year);
				}
			}
		}
	}

	public function AddThesis($year){
		if(isset($this->__params['POST']['thesis'])){
			$error=0;
			$thesis =  $this->__params['POST']['thesis']; 
			$promoter=explode(" ",$thesis[0]);
			if(count($promoter)>1){
				$promoter_i=count($promoter)-1;
				$name=$promoter[$promoter_i-1];
				$surname=$promoter[$promoter_i];
				$promoterId = $this->__db->execute("SELECT * FROM promoter WHERE name='".$name."' AND surname ='".$surname."' LIMIT 1");
				if($promoterId==NULL){
					$qualification=$promoter[0];

					for($i=1; $i < $promoter_i-1; $i++)
					{
						$qualification=$qualification." ".$promoter[$i];
					}
					$res = $this->__db->execute("INSERT INTO promoter VALUES (NULL,'{$qualification}','{$name}','{$surname}')");

					$promoterId = $this->__db->execute("SELECT * FROM promoter WHERE name='".$name."' AND surname ='".$surname."' LIMIT 1")[0]['id'];
				}
				else{
					$promoterId=$promoterId[0]['id'];
				}
			}
			else{
				$error=1;
				$alert=$this->getAlert(0, "Wystąpił bład", "Nie udało się dododać tematu, sprawdź poprawność pliku.");
				echo $alert;
			}

			$email =NULL;
			$album =NULL;
			$studentID=NULL;
			$idThesis="";
			if(isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) )
			{
				$email = htmlentities($this->__params['POST']['email'], ENT_QUOTES);
				$album = htmlentities($this->__params['POST']['album'], ENT_QUOTES);

				$studentID = $this->__db->execute("SELECT id FROM student WHERE nr_index ='".$album."' LIMIT 1");
				if($studentID!=NULL){
					$error=1;
					$alert=$this->getAlert(0, "Temat nie został dodany", "Ten student już posiada wybrany temat.");
					echo $alert;
				}
			}
			
			if(isset($promoterId) && $promoterId!=Null){
				$idThesis = $this->__db->execute("SELECT *
				FROM thesis
				WHERE id_promoter='".$promoterId."' AND topic ='".$thesis[1]."'AND topic_in_english ='".$thesis[2]."' AND id_year ='".$year['id']."' LIMIT 1");	
			}

			if($idThesis!=NULL){
				$error=1;
				$alert=$this->getAlert(0, "Temat został już wcześniej dodany", "Nie można dodać kolejny raz tego samego tematu.");
				echo $alert;
			}
			
			if($error==0){
				$url_key = NULL;
				$access_code=bin2hex(random_bytes (4));
				$today = date("Y-m-d H:i:s"); 

				$res = $this->__db->execute("INSERT INTO reservation VALUES (NULL,NULL,'dostępny','{$today}',NULL,NULL)");
				$reservationId = $this->__db->execute("SELECT LAST_INSERT_ID()")[0]['LAST_INSERT_ID()'];

				$typeYear = $this->__db->execute("SELECT type_of_studies FROM year WHERE id='".$year['id']."' LIMIT 1")[0];
				if($typeYear['type_of_studies']=="licencjackie"){
					$res = $this->__db->execute("INSERT INTO thesis VALUES (NULL,'{$year['id']}','{$promoterId}','{$thesis[1]}','{$thesis[2]}','{$thesis[3]}',NULL,'{$thesis[4]}','{$thesis[5]}','{$thesis[6]}','{$thesis[7]}','{$reservationId}')");
				}
				else{
					$res = $this->__db->execute("INSERT INTO thesis VALUES (NULL,'{$year['id']}','{$promoterId}','{$thesis[1]}','{$thesis[2]}','{$thesis[3]}','{$thesis[4]}','{$thesis[5]}','{$thesis[6]}','{$thesis[7]}','{$thesis[8]}','{$reservationId}')");	
				}
				$idThesis=$this->__db->execute("SELECT LAST_INSERT_ID()")[0]['LAST_INSERT_ID()'];
				if(!isset($idThesis)){
					$error=1;
					$alert=$this->getAlert(0, "Wystąpił bład", "Nie udało się dododać tematu, sprawdz poprawność pliku.");
					echo $alert;
				}
				else if(isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) ){
					$email = htmlentities($this->__params['POST']['email'], ENT_QUOTES);
					$album = htmlentities($this->__params['POST']['album'], ENT_QUOTES);
					if($studentID==NULL)
					{
						$res = $this->__db->execute("INSERT INTO student VALUES (NULL,NULL,NULL,'{$email}','{$album}',NULL,NULL,NULL,NULL,NULL)");
						$studentID = $this->__db->execute("SELECT id FROM student WHERE email='".$email."' AND nr_index ='".$album."' LIMIT 1")[0]['id'];
						$url_key=(bin2hex(random_bytes (5)));
						$res = $this->__db->execute("UPDATE reservation SET id_student='{$studentID}', status = 'wstępnie zarezerwowany', date ='{$today}', url_key='{$url_key}', access_code = '{$access_code}'  WHERE id= '{$reservationId}'");
						$addresWebThesis=$_SERVER['SERVER_NAME'].$this->getProjectCatalogPath().'thesis/save/'.$idThesis.'/'.$url_key;
						$status_wyslane=$this->sendEmail($email,$addresWebThesis,$access_code);
						$status_wyslane=1;
					}
				}
			}
			if($error==0){
				$alert=$this->getAlert(1, "Temat został dodany", "Powinnien się pojawić na liscie z tematami.");
				echo $alert;
			}
			echo'
			<div class="row">
				<div class="col-12">
					<p class="mb-0 text-justify">Jeżeli chcesz możesz dodać kolejny tematy, lub zakonczyć pracę i się wylogować. </p>
					<div class="col-6 col-sm-6 float-left p-0">
						<a href ="'.$this->getProjectCatalogPath().'thesis/add/'.$year['id'].'" class="btn btn-info p-2 mt-2 mb-2">Dodaj kolejny temat</a>
					</div>	
					<div class="col-6 col-sm-6 float-right  p-0">
					<a href ="'.$this->getProjectCatalogPath().'logout"  class="btn btn-warning p-2 mt-2 mb-2 float-right">WYLOGUJ</a>	
				</div>
				</div>
			</div>
			';
		}
	}

	public function getTableThesis($year){		
		echo '<h3 class="text-dark m-2 mb-4 text-center text-uppercase">Rok akademicki '.$year['years'].'.</h3>
		<h3 class="text-dark m-2 mb-4 text-center text-uppercase"> Studia: '.$year['type_of_studies'].'</h3>';

			echo'<h4 class="text-uppercase">Prezentacja danych</h4>
			<div class="row">
				<div class="col-md-12">';
				
				echo '<p>Odebrano plik: '.$_FILES['file']['name'].'</p>';
				
				$e = registry::register("phpoffice");
				$data=$e->LoadFileData($_FILES['file']['name'], $_FILES['file']['tmp_name']);		
				$i=0;
				$error=0;
				if($data==0){
					echo '<p class="mb-5 pb-4" >Niepoprawny plik</p>';
					$error=1;
				}
				else if($year['type_of_studies']=="magisterskie" && 9!=count($data)){
						$alert=$this->getAlert(0, "Wystąpił bład", "Niepoprawny plik dla studiów magisterskich");
						echo $alert;
						$error=1;
				}
				elseif ($year['type_of_studies']=="licencjackie" && 8!=count($data)){
						$alert=$this->getAlert(0, "Wystąpił bład", "Niepoprawny plik dla studiów licencjackich");
						echo $alert;
						$error=1;
				}
				else{
				echo '
					<div class="table-responsive">
						<table class="table-light text-dark ">
							<tr class="table-primary">
								<td class="col-1 col-sm-3 p-md-4">Promotor:</td>
								<td class="col-11 col-sm-9 p-md-4">'.$data[$i].'</td>
							</tr>
							<tr>
								<td class="p-md-4">Temat pracy dyplomowej <br /> (j. polski):</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
							<tr>
								<td class="p-md-4">Temat pracy dyplomowej <br /> (j. angielski):</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
							<tr>
								<td class="p-md-4">Zakres pracy i oczekiwane rezultaty praktyczne:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>';
						if($year['type_of_studies']=="magisterskie"){
							echo '
							<tr>
								<td class="p-md-4">Aspekt naukowy, problemowy, innowacyjny pracy:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>';
						};

							echo'
							<tr>
								<td class="p-md-4">Wymagane oprogramowanie/ języki programowania:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
							<tr>
								<td class="p-md-4">Środowisko uruchomieniowe:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
								<tr>
								<td class="p-md-4">Dodatkowe wymagania i uwagi:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
							<tr>
								<td class="p-md-4">Literatura:</td>
								<td class="p-md-4">'.$data[++$i].'</td>
							</tr>
					</table>
					</div>';
				}
				echo'
				</div>
			</div>
			<form action="" method="POST">';
		
			if(isset($this->__params['POST']['add_student']) && !empty($this->__params['POST']['add_student'])&& $this->__params['POST']['add_student']=="Tak" && $error==0)
			{
				echo'						
				<h3 class="text-uppercase pt-3 pb-3">Dane studenta - rezerwacja</h3>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="email" class="form-control" placeholder="Enter email" required>
						</div>
						<div class="form-group">
							<label>Numer albumu (legitymacji)</label>
							<input type="text" name="album" class="form-control"  placeholder="Numer albumu" required>
						</div>
						<input type="hidden" name="message" value="1">
					</div>
					<div class="col-md-6">';
						$alert=$this->getAlert(-1, "Uwaga!", "Na emaila studenta zostanie wysłana wiadomość umożliwijaca mu kontynuację rezerwacji tego tematu pracy dyplomowej.");
						echo $alert.'
					</div>	
				</div>';
			}

				echo'	
					<div class="row pt-sm-3">
						<div class="col-6 col-sm-3 float-left">
							<a href ="'.$this->getProjectCatalogPath().'thesis/add/'.$year['id'].'" class="btn btn-info p-2 mt-2 mb-2 float-left">COFNIJ</a>	
						</div>
						<div class="col-6 col-sm-3 float-left">
						
						<a href ="'.$this->getProjectCatalogPath().'logout"  class="btn btn-warning p-2 mt-2 mb-2 float-left">WYLOGUJ</a>	
					</div>
						<div class="col-12 col-sm-6 float-right">	';
						if($error==0){					
							foreach($data as $id) {
								echo '<input type="hidden" name="thesis[]" value="'.$id.'" />';
							}
							echo'
								<button type="submit" class="btn btn-success float-right p-2 mt-2 mb-2">POTWIERDŹ DODANIE TEMATU</button>';
						}
					echo '</div>';
				echo '</div>';	
			echo '</div>';	
		echo '</form>';
	}			
}

?>