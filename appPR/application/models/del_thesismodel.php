<?php

class del_thesismodel extends generalmodel
{
	public function delThesis(){
		if(isset($this->__params['POST']['thesis']))
		{
			$idThesis= htmlentities($this->__params['POST']['thesis'], ENT_QUOTES);

			$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status,id_student,id_reservation FROM thesis
			LEFT JOIN year on year.id = thesis.id_year 
			LEFT JOIN promoter on promoter.id = thesis.id_promoter
			LEFT JOIN reservation on reservation.id = thesis.id_reservation
			WHERE thesis.id='".$idThesis."'LIMIT 1");
			if(isset($thesis) && !empty($thesis))
			{
				$thesis=$thesis[0];
			
				$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status,id_student,thesis.id_reservation FROM thesis
				LEFT JOIN year on year.id = thesis.id_year
				LEFT JOIN promoter on promoter.id = thesis.id_promoter
				LEFT JOIN reservation on reservation.id = thesis.id_reservation
				WHERE thesis.id='".$idThesis."'LIMIT 1")[0];

				
				$id_student= $this->__db->execute("SELECT id_student FROM reservation WHERE id='".$thesis['id_reservation']."' LIMIT 1")[0]['id_student'];

				$this->__db->execute("DELETE FROM thesis WHERE id='{$idThesis}' ");
				$this->__db->execute("DELETE FROM reservation WHERE id_student='{$thesis['id_student']}' ");
				if(isset($id_student)){
					$this->__db->execute("DELETE FROM student WHERE id='{$id_student}' ");
				}	
				
			}

			$alert=$this->getAlert(1, "Temat został usunięty", "");
			echo $alert;
			echo '<div class="m-3 text-center">
                    <a href="'.$this->getProjectCatalogPath().'administrator" class="btn btn-info p-2 my-2 m-md-2" >Wróć na stronę główną</a>
                </div>';
		}
		else{
			$this->goToErorrPage();
		}
	}
}
?>