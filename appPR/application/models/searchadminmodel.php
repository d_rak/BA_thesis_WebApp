<?php

class searchadminmodel
{
    private $__config;
    private $__router;
    private $__db;
    private $__params;
    
    public function __construct()
    {
        $this->__config = registry::register("config");
        $this->__router = registry::register("router");
        $this->__db = registry::register("db");
        $this->__params = $this->__router->getParams();
    }
    
    public function getSearch(){
        if(isset($_GET["search"])) {
            $search = htmlentities($_GET["search"], ENT_QUOTES);
            if(strlen ($search)>=3 && strlen($search)<=25){
                echo '<h3 class="pb-3">Wyszukiwana fraza : '.$search.'</h3>';
                
                $search_int =intval($search);
                $theses='';
                if ($search_int!=0) {
                    $search=$search_int;
                    $theses = $this->__db->execute("SELECT thesis.id,id_promoter,topic, years,year.type_of_studies,qualification,promoter.name,promoter.surname,status,id_student,nr_index FROM thesis 
                    LEFT JOIN year on year.id = thesis.id_year 
                    LEFT JOIN promoter on promoter.id = thesis.id_promoter 
                    LEFT JOIN reservation on reservation.id = thesis.id_reservation 
                    LEFT JOIN student on student.id = reservation.id_student 
                    WHERE nr_index ='".$search."'");
                }
                else{
                    $theses = $this->__db->execute("SELECT thesis.id,id_promoter,topic, years,type_of_studies,qualification,name,surname,status FROM thesis 
                    LEFT JOIN year on year.id = thesis.id_year 
                    LEFT JOIN promoter on promoter.id = thesis.id_promoter
                    LEFT JOIN reservation on reservation.id = thesis.id_reservation
                    WHERE topic LIKE '%".$search."%' ");
                }

                if( $theses==NULL){
                    echo '<h4 class="pb-3">Brak wyników</h4>';
                    $this->getFormSearch();
                }
                $promoter='';
                foreach($theses as $thesis)
                {
                    $shortTypeThesis='';
                    if($thesis['type_of_studies']=='licencjackie'){
                        $shortTypeThesis='ba';
                    }
                    else{
                        $shortTypeThesis='ma';
                    }

                    if($promoter!=$thesis['id_promoter']){
                        $promoter=$thesis['id_promoter'];
                        echo '
                        <div class="row table-primary text-center">
                            <div class="col ">
                                <p class="m-3 m-sm-3">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</p>
                            </div>
                        </div>';
                    }
                    echo'
                    <div class="row table-light p-lg-3 p-1 border border-top-0">
                        <div class="col-md-12 col-lg-2 align-self-center text-justify px-2">
                            <p class="m-md-auto m-0">Studia '.$thesis['type_of_studies'].'</p>
                        </div>
                        <div class="col-md-5 col-lg-5 align-self-center text-justify px-2">
                            <a href="administrator/show/'.$shortTypeThesis.'/'.$thesis['id'].'" >'.$thesis['topic'].'</a>
                        </div>
                        <div class="col-md-4 col-lg-3 text-center px-2 m-0 align-self-center">';
                        if($thesis['status']=='dostępny'){
                            $type_button='success';
                            $status_button='';
                        }
                        else if($thesis['status']=='zarezerwowany'){
                            $type_button='danger';
                            $status_button='disabled';
                        }
                        else{
                            $type_button='warning';
                            $status_button='disabled';
                        } 
                        echo'<a href="thesis/res/'.$shortTypeThesis.'/'.$thesis['id'].'" class="btn btn-'.$type_button.' '.$status_button.' p-2 mt-2 mb-2" role="button" >'. $thesis['status'].'</a>
                        </div> 
                        <div class="col-md-3 col-lg-2 align-self-center text-center px-2 m-0">';
						if($thesis['status']=='wstępnie zarezerwowany'){
							echo '<a href="administrator/accept/'.$shortTypeThesis.'/'.$thesis['id'].'" class="btn btn-success p-2 mt-2 mb-2" role="button" >ZATWIERDŹ</a>';
						}
						echo'
						</div>
                    </div>';
                }
            }
            else{
                echo'<p class="text-danger">Treść musi mieć minimum 3 znaki, a maksymalnie 25 znaków</p> ';
                $this->getFormSearch();
            }
        }
        else{
            $this->getFormSearch();
        }
    }
    public function getFormSearch(){
        echo'   
        <p>Wpisz czego szukasz</p> 
        <form action="" method="GET">
        <div class="row" >

                <div class="col-12 col-sm-8">
                    <input class="form-control" type="search" placeholder="Szukaj tematu" aria-label="Search" required name="search">
                </div>
                <div class="col-12 col-sm-4 text-right">
                    <button class="btn btn-info my-2 my-sm-0" type="submit">Szukaj</button>
                </div>

        </div>            
        </form>';
    }
}

?>