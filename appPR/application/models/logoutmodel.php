<?php

class logoutmodel
{
	private $__config;
	
	public function __construct()
	{
		$this->__config = registry::register("config");
		if(isset($_SESSION[$this->__config->default_session_auth_var]))
		{
			unset($_SESSION[$this->__config->default_session_auth_var]);
			header("Location: ".SERVER_ADDRESS);
		}
		if(isset($_SESSION['initiated']) && !empty($_SESSION['idStudent'])){
			unset($_SESSION['initiated']);
			unset($_SESSION['idStudent']);
			session_destroy();
		}
		elseif (isset($_SESSION['initiatedPromotor']) && !empty($_SESSION['idYear'])){
			unset($_SESSION['initiatedPromotor']);
			unset($_SESSION['idYear']);
			session_destroy();
		}
		header("Location: ".SERVER_ADDRESS);
	}
}
?>