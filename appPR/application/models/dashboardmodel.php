<?php

class dashboardmodel extends generalmodel
{
		
	public function getAllYearsThesis()
	{
		$years = $this->__db->execute("SELECT year.id, year.years, year.type_of_studies FROM year  
		ORDER BY years DESC, type_of_studies");
		return $years;

	}
	public function getCountThesis($yearId)
	{
		$CountThesis = $this->__db->execute("SELECT COUNT(id_year) FROM thesis  
		WHERE id_year='".$yearId."'")[0];
		return $CountThesis["COUNT(id_year)"];

	}
	public function getCountThesisAvailable($yearId)
	{
		$CountThesis = $this->__db->execute("SELECT COUNT(id_year) FROM thesis  
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE id_year='".$yearId."' AND status  ='dostępny' ")[0];
		return $CountThesis["COUNT(id_year)"];
	}
	public function getCountThesisPreBooked($yearId)
	{
		$CountThesis = $this->__db->execute("SELECT COUNT(id_year) FROM thesis  
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE id_year='".$yearId."' AND status  ='wstepnie zarezerwowany' ")[0];
		return $CountThesis["COUNT(id_year)"];

	}
	public function getCountThesisReserved($yearId)
	{
		$CountThesis = $this->__db->execute("SELECT COUNT(id_year) FROM thesis  
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE id_year='".$yearId."' AND status  ='zarezerwowany' ")[0];
		return $CountThesis["COUNT(id_year)"];

	}
	public function getStatistics(){	
		$years=$this->getAllYearsThesis();
		echo '
		<div class="table-responsive">
		<table class="table-light text-dark">';
		echo'
		<tr class="table-primary text-center">
			<td class="col-md-2">Rok akademicki</td>
			<td class="col-md-2">Studia</td>
			<td class="col-md-2">Ilosc tematów ogolnie</td>
			<td class="col-md-2 bg-success">Ilosc tematów dostepnych</td>
			<td class="col-md-2 bg-warning">Ilosc tematów wstepnie zarezerwowanych</td>
			<td class="col-md-2 bg-danger">Ilosc tematów zarezerwowanych</td>
		</tr>';
		foreach($years as $year)
		{
			$CountThesis=$this->getCountThesis($year['id']);
			$CountThesisAvailable=$this->getCountThesisAvailable($year['id']);
			$CountThesisPreBooked=$this->getCountThesisPreBooked($year['id']);
			$CountThesisReserved=$this->getCountThesisReserved($year['id']);
				echo'
					<tr class="text-center"> 
						<td class="col-md-2"><a href="'.$this->getProjectCatalogPath().'administrator/year/'.$year['id'].'">'.$year['years'].'</a></td>
						<td class="col-md-2">'.$year['type_of_studies'].'</td>
						<td class="col-md-2">'.$CountThesis.'</td>
						<td class="col-md-2">'.$CountThesisAvailable.'</td>
						<td class="col-md-2">'.$CountThesisPreBooked.'</td>
						<td class="col-md-2">'.$CountThesisReserved.'</td>
					</tr>';
		}
		echo '</table></div>';
	}
}
?>
