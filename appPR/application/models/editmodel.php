<?php

class editmodel extends generalmodel
{

	public function sendEmail($idStudent,$url_key){
		$idThesis=$this->getIdThesis(0);
		$access_code = $this->__db->execute("SELECT access_code
		FROM reservation
		WHERE url_key='".$url_key."' LIMIT 1")[0]["access_code"];
		$student_email = $this->__db->execute("SELECT email FROM student WHERE id='".$idStudent."' LIMIT 1")[0]['email'];

		 if(isset($_SESSION['sendEmail'])){				
			if($_SESSION['sendEmail']==true){
				$addresWebThesis=SERVER_ADDRESS.'thesis/save/'.$idThesis.'/'.$url_key;
				$e = registry::register("mailer");
				$status_wyslane=$e->MailGenerator($student_email,$addresWebThesis,$access_code); 
				if($status_wyslane==true){
						$alert=$this->getAlert(1, "Temat został zarezerwowany", "Na emaila została wysłana widomość potwierdzająca rezerwację tematu. Wypełnij spokojnie resztę danych.");
						echo $alert;
						$_SESSION['sendEmail']= 0;
				}
				else{
					$title="Wystąpił bład";
					$alert=$this->getAlert(1, "Temat zostal zarezerwowany","Wypełnij spokojnie resztę danych,");
					echo $alert;
					$alert=$this->getAlert(0, "Nie wysłano emaila", "Email nie został wysłany, uzupełnij dane, skorzystaj z pomocy FAQ lub skontaktuj się z sekretariatem z prośbą o recznie wysłanie maila.");
					echo $alert;
					$alert=$this->getAlert(-1, "Zapisz sobie dane:", "Rezerwacja jest dostępna pod adresem: ".$addresWebThesis." <br>Twój kod dostępu: <strong>".$access_code."</strong>");
					echo $alert;
				}
			}
		}
	}

	public function getDataStudent(){
		$idThesis=$this->getIdThesis(0);
		$now = time();
		$expiryTime = 1800;
		$url_key = htmlentities($this->__params[1], ENT_QUOTES);

		$res = $this->__db->execute("SELECT url_key,id_student
		FROM reservation
		WHERE url_key='".$url_key."' LIMIT 1");
		if($res==Null){
			$this->goToErorrPage();
		}
		else{
			$res=$res[0];
		}

		if (!isset($_SESSION['initiated']) && isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) && isset($this->__params['POST']['code']))
		{
			if(isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) )
			{
				$email =  htmlentities($this->__params['POST']['email'], ENT_QUOTES);
				$album =  htmlentities($this->__params['POST']['album'], ENT_QUOTES);
				$code =  htmlentities($this->__params['POST']['code'], ENT_QUOTES);

				$idStudent = $this->__db->execute("SELECT id FROM student WHERE email='".$email."' AND nr_index ='".$album."' LIMIT 1")[0];

				$res = $this->__db->execute("SELECT id_student
				FROM reservation
				WHERE url_key='".$url_key."' AND access_code='".$code."' AND id_student='".$idStudent['id']."' LIMIT 1");

				if($res==Null || $idStudent==Null){
					$alert=$this->getAlert(0, " Nie poprawne dane", "Wprowadz poprawne dane, takie jakie zostały wysłane na emaila");
					echo $alert;
				}
				else{
					$res=$res[0];
					
					$now = time();
					$expiryTime = 1800;
					$_SESSION['initiated'] = true;
					$_SESSION['idStudent'] = $idStudent['id'];
					if (isset($_SESSION['initiated']) && isset($_SESSION['idStudent']))
					{
						session_regenerate_id();
						$_SESSION['lastTrace'] = $now;
						$_SESSION['initiated'] = true;
						$_SESSION['idStudent'] = $idStudent['id'];
						$_SESSION['sendEmail'] = false;
						$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
					}
				}
			}
		}

		if (!isset($_SESSION['initiated']) && !isset($_SESSION['idStudent']))
		{
			$this->getFormLoginStudent();
		}
		else if ((int)$_SESSION['lastTrace'] + $expiryTime < $now){
			$sessionName = session_name();    
			$_SESSION = array();
			if (isset($_COOKIE[$sessionName]))
			{
				setcookie($sessionName, '', $now-3600, '/');
			}
			session_destroy();
			header("Location:");
		}
		else{
			if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']){
				session_destroy();
				header("Location:");
			}
			else if($_SESSION['idStudent'] != $res['id_student']){
				$this->getFormLoginStudent();
			}
			else{
				$this->getFormStudent($idThesis);
			}
		}
		
	}
	
	public function getFormLoginStudent(){
		echo '
			<form action="" method="POST">
				<fieldset> 
					<legend>Dane studenta</legend>
					<div class="row">
						<div class="col-md-6">	
								<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" placeholder="Enter email" required>
							</div>
							<div class="form-group">
								<label>Numer albumu (legitymacji)</label>
								<input type="text" name="album" class="form-control" pattern="\d*"  placeholder="Numer albumu" required>
							</div>	
							<div class="form-group">
								<label>Podaj hasło</label>
								<input type="password" name="code" class="form-control" placeholder="kod dostepu" required>
							</div>
						</div>
						<div class="col-md-6">';
						echo $this->getAlert(-1, "Weryfikacja dostępu!", "Dane potrzebne do logowania są w widomosci email");
							echo'
						</div>
					</div>';
				$type_button='success';
				echo'
					<button type="submit" class="btn btn-'.$type_button.' float-right p-2 mt-2 mb-2">ZALOGUJ</button>
				</fieldset>
			</form>
		';
	}

	public function getFormStudent($idThesis){

		$url_key = htmlentities($this->__params[1], ENT_QUOTES);
		$idStudent  = $this->__db->execute("SELECT id_student
		FROM reservation
		WHERE url_key='".$url_key."' LIMIT 1")[0]["id_student"];

		$student = $this->__db->execute("SELECT * FROM student WHERE id='".$idStudent."' LIMIT 1")[0];

		$url_key = htmlentities($this->__params[1], ENT_QUOTES);
		$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status 
		FROM thesis
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE thesis.id='".$idThesis."'LIMIT 1")[0];

		if($thesis==NULL || $student==NULL){
			$this->goToErorrPage();
		}
		$this->sendEmail($idStudent,$url_key);
		echo '						
		<form action="'.$this->getProjectCatalogPath().'thesis/save/'.$idThesis.'/'.$url_key.'" method="POST">
			<fieldset> 
				<legend>Dane studenta</legend>
				<div class="row">
					<div class="col-md-7">
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="email" class="form-control " style="pointer-events: none" readonly  value="'.$student['email'].'">
						</div>
						<div class="form-group">
							<label>Numer albumu (legitymacji)</label>
							<input type="text" name="album" class="form-control"  style="pointer-events: none" readonly value="'.$student['nr_index'].'">
						</div>
						<div class="form-group">
							<label>Imię</label>
							<input type="text" name="name" class="form-control" placeholder="Imię" required value="'.$student['name'].'">
						</div>
						<div class="form-group">
							<label>Nazwisko</label>
							<input type="text" name="surname" class="form-control" placeholder="Nazwisko" value="'.$student['surname'].'" required>
						</div>
						<div class="form-group w-50 float-left pr-sm-2">
							<label for="exampleSelect1">Rok studiów</label>
							<select class="form-control" id="exampleSelect1" name="year_of_study">
								<option>I</option>
								<option>II</option>
								<option>III</option>
							</select>
						</div>
						<div class="form-group w-50 float-right">
							<label for="exampleSelect1">Studia</label>
							<select class="form-control" id="exampleSelect1" name="level_studies">
								<option>Licencjackie</option>
								<option>Magisterskie</option>
							</select>
						</div>
						<div class="form-group w-50 float-left pr-sm-2">
							<label for="exampleSelect1">Typ studiów</label>
							<select class="form-control" id="exampleSelect1" name="type_of_studies">
								<option>Stacjonarne</option>
								<option>Niestacjonarne</option>
							</select>
						</div>
						<div class="form-group w-50 float-right">
							<label>Kierunek</label>
							<input type="text" name="field_of_study" class="form-control" placeholder="Np. Informatyka, Matematyka" required value="'.$student['field_of_study'].'">
						</div>
						<div class="form-group w-100 float-left m-0">
							<label>Specjaność</label>
							<input type="text" name="specialty" class="form-control" placeholder="Np. Multimedia i technologie internetowe" value="'.$student['specialty'].'">
						</div>
					</div>
					<div class="col-md-5">';
						echo $this->getAlert(2, "Informacja","Dane email i numer albumu są zablokowane do edycji. <br /><br /> Reszta danych jest możliwa do zmiany.").'
					</div>
				</div>';
			echo'
			<div class="row pt-sm-3">
				<div class="col-6 float-left">
				</div>
				<div class="col-6 float-right">	
					<input type="hidden" name="save" value="1">
					<button type="submit" class="btn btn-success float-right p-2 mt-2 mb-2">ZAPISZ DANE</button>
				</div>
			</div>
		</fieldset>';

		echo '</form>';
		echo '
		<div class="mb-2 mb-sm-0">
			<div class="progress m-auto border border-secondary"  style="min-width:150px; width: 25%;">
					<div class="progress-bar bg-success" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"><p class="m-0 p-0">Krok 1 / 2</p></div>
			</div>
		</div>';
	}
}
?>