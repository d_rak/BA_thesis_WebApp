<?php

class administratormodel 
{
	private $__config;
    private $__router;
    private $__db;
    private $__params;
	private $alert;
	private $alertType;
	private $login;
	public function __construct()
	{
		$this->__config = registry::register("config");
        $this->__router = registry::register("router");
        $this->__db = registry::register("db");
		$this->__params = $this->__router->getParams();
		
		if(isset($this->__params['POST']['login']))
		{	
			$this->login = htmlentities($this->__params['POST']['login'], ENT_QUOTES);		//ENT_QUOTES Konwertuje  podwójne i pojedyncze cudzysłowy.
			$q = $this->__db->execute("SELECT * FROM user_secretariat WHERE user_name ='".$this->login."' LIMIT 1");
			if(!empty($q))
			{
				if(password_verify($this->__params['POST']['pass'],$q[0]['password'])){
					$_SESSION[$this->__config->default_session_admin_auth_var] = $this->__params['POST']['login'];
				}
				else{
					$this->alert = "Nieprawidłowy login lub haslo";
					$this->alertType = "danger";
				}
			}
			else{
				$this->alert = "Nieprawidłowy login lub haslo";
				$this->alertType = "danger";
			}
		}
		
		if(!isset($_SESSION[$this->__config->default_session_admin_auth_var]))
		{
			if($this->__router->getAction() != "index")
			{
				header("Location: ".SERVER_ADDRESS."administrator/index");
			}
		}
		else
		{
			if(isset($this->__params['POST']['login']) || $this->__router->getAction() == "index")
			{
				header("Location: ".SERVER_ADDRESS."administrator/dashboard");
			}
		}
	}

	public function getLoggedUser()
	{
		$q = $this->__db->execute("SELECT * FROM user_secretariat WHERE user_name = '".$_SESSION[$this->__config->default_session_admin_auth_var]."' LIMIT 1");
		return $q[0]['user_name'];
	}

	function getAlert(){
		return $this->alert;
    }

	function getAlertalertType(){
		return $this->alertType;
	}
	
	function getLogin(){
        if(isset($this->login)){
            return $this->login;
        }
        else{
            return '';
        }
    }

	public function logout()
	{
		if(isset($_SESSION[$this->__config->default_session_admin_auth_var])) 
			unset($_SESSION[$this->__config->default_session_admin_auth_var]);

		header("Location: ".SERVER_ADDRESS."administrator/index");
	}
}
?>