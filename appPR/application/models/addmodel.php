<?php

class addmodel extends generalmodel
{
	public function drawAddThesis(){
		$idYear="";
		if(isset($this->__params[0]) && !empty($this->__params[0]))
		{
			$idYear=htmlentities($this->__params[0], ENT_QUOTES);
		}
		if(isset($this->__params['POST']['code']) && !empty($this->__params['POST']['code']))
		{
			$code = htmlentities($this->__params['POST']['code'], ENT_QUOTES);			
		
			$year = $this->__db->execute("SELECT *
			FROM year
			WHERE id='".$idYear."' AND access_code='".$code."' LIMIT 1");
			if($year==Null || $idYear==Null){
				$alert=$this->getAlert(0, "Brak dostępu", "Błedny kod dostepu bądź link dostępu");
				echo $alert;
			}
			else{
				$now = time();
				$expiryTime = 1800;
				$_SESSION['initiatedPromotor'] = true;
				$_SESSION['idYear'] = $idYear;
				if (isset($_SESSION['initiatedPromotor']) && isset($_SESSION['idYear']))
				{
					session_regenerate_id();
					$_SESSION['lastTrace'] = $now;
					$_SESSION['initiatedPromotor'] = true;
					$_SESSION['idYear'] = $idYear;
					$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
				}
				
			}
		}
		$now = time();
		$expiryTime = 1800;
		if (!isset($_SESSION['initiatedPromotor']) && !isset($_SESSION['idYear']))
		{
			$this->getFormLoginAddThesis();
		}
		else if ((int)$_SESSION['lastTrace'] + $expiryTime < $now){
			$sessionName = session_name();    
			$_SESSION = array();
			if (isset($_COOKIE[$sessionName]))
			{
				setcookie($sessionName, '', $now-3600, '/');
			}
			session_destroy();
			header("Location:");
		}
		else{
			if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']){
				session_destroy();
				header("Location:");
			}
			else if($_SESSION['idYear'] != $idYear){
				$this->getFormLoginAddThesis();
			}
			else{
				$year = $this->__db->execute("SELECT type_of_studies
				FROM year
				WHERE id='".$idYear."' LIMIT 1");
				if($year==null){
					$this->goToErorrPage();
				}
				else{
					$year=$year[0];
				}
				$this->getFormAddThesis($idYear,$year['type_of_studies']);
			}
		}
	}

	public function getFormAddThesis($idYear,$typeOfStudies){
		echo'
		<form enctype="multipart/form-data" action="'.$this->getProjectCatalogPath().'thesis/new/'.$idYear.'" method="POST">
			<h4 class="text-uppercase">Dodaj temat pracy dyplomowej</h4>
			<div class="row">
				<div class="col-md-6">	
					<div class="form-group pt-2 pb-2">
						<label>Wybierz plik:</label><br>
						<input type="file" id="file" name="file" accept=".docx" required>
					</div>
					<div>
						<h5 class="pt-2 pb-2">Studia: '.$typeOfStudies.'</h5>
					</div>
					<div class="form-group  pt-2 pb-2">
						<label for="exampleSelect1">Temat zarezerwowany dla studenta</label>
						<select class="form-control" id="exampleSelect1" name="add_student">
							<option>Nie</option>
							<option>Tak</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">';
					$alert=$this->getAlert(-1, "Uwaga!", "Plik powinien być przygotowany zgodnie z szablonem, i posiadać roszerzenie.docx <br><br>
					Sprawdź poprawność danych, gdyż nie można ich poźniej edytować.<br><br> 
					Temat usunąć może jedynie sekretaria");
					echo $alert.'
				</div>		
			</div>
			<div class="row">
				<div class="col-6 float-left">
					<a href ="'.$this->getProjectCatalogPath().'logout"  class="btn btn-warning p-2 mt-2 mb-2">WYLOGUJ</a>	
				</div>
				<div class="col-6 float-right">				
					<div class="text-right">
						<button type="submit" class="btn btn-success float-right p-2 mt-2 mb-2">DODAJ TEMAT</button> 
					</div>
				</div>
			</div>
	</form>	';
	}

	public function getFormLoginAddThesis(){
	echo '
	<div class="container">		
		<form action="" method="POST">
			<fieldset> 
				<div class="row">
					<div class="col-md-6">	
						<div class="form-group">
							<label>Podaj hasło</label>
							<input type="password" name="code" class="form-control" placeholder="kod dostepu" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="alert alert-dismissible alert-warning">
							<h4 class="alert-heading">Weryfikacja dostepu!</h4>
							<p class="mb-0">Kod dostepu jest w widomosci email</p>
						</div>
					</div>
				</div>
					<button type="submit" class="btn btn-success float-right p-2 mt-2 mb-2">ZALOGUJ</button>
				</fieldset>
			</form>
		</div>';
	}	
}
?>