<?php

class del_resmodel  extends generalmodel
{
	public function del_reservation(){
		if(isset($this->__params['POST']['thesis']) && isset($this->__params['POST']['url_key'])){
			$idThesis= htmlentities($this->__params['POST']['thesis'], ENT_QUOTES);
			$url_key= htmlentities($this->__params['POST']['url_key'], ENT_QUOTES);

			$id_reservation = $this->__db->execute("SELECT id_reservation FROM thesis WHERE id='".$idThesis."' LIMIT 1")[0];
			$id_student= $this->__db->execute("SELECT id_student FROM reservation WHERE id='".$id_reservation['id_reservation']."' AND url_key ='".$url_key."' LIMIT 1");
			if(isset($id_student) && !empty($id_student)){
				$id_student= $this->__db->execute("SELECT id_student FROM reservation WHERE id='".$id_reservation['id_reservation']."' AND url_key ='".$url_key."' LIMIT 1")[0];	
				$today = date("Y-m-d H:i:s");
				$res = $this->__db->execute("UPDATE reservation SET id_student=NULL, status = 'dostępny', date ='{$today}', url_key=NULL, access_code =NULL  WHERE id= '{$id_reservation['id_reservation']}'");
				$this->__db->execute("DELETE FROM student WHERE id = '{$id_student['id_student']}'");
			}

			$alert=$this->getAlert(1, "Zrezygnowano z tematu", "Tema został zwolniony.");
			echo $alert;
			echo '<div class="m-3 text-center">
                    <a href="'.$this->getProjectCatalogPath().'" class="btn btn-info p-2 my-2 m-md-2" >Wróć na stronę główną</a>
				</div>';
				
				session_destroy();
				if (isset($_SESSION['initiated']))
				{
					unset($_SESSION['lastTrace']);
					unset($_SESSION['initiated']);
					unset($_SESSION['idThesis']);
					unset($_SESSION['idStudent']);
					unset($_SESSION['sendEmail']);
					unset($_SESSION['ip']);
				}
		}
		else{
			$this->goToErorrPage();
		}
	}
}
?>