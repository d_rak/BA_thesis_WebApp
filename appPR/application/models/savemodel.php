<?php

class savemodel extends generalmodel
{

	public function drawDataStudent(){
		
		$idThesis=$this->getIdThesis(0);
		$now = time();
		$expiryTime = 1800;
		$url_key = htmlentities($this->__params[1], ENT_QUOTES);

		$res = $this->__db->execute("SELECT url_key,id_student
		FROM reservation
		WHERE url_key='".$url_key."' LIMIT 1");
		
		if($res==Null){
			$this->goToErorrPage();
		}
		else{
			$res=$res[0];
		}
		if (!isset($_SESSION['initiated']) && isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) && isset($this->__params['POST']['code']))
		{
			$email =  htmlentities($this->__params['POST']['email'], ENT_QUOTES);
			$album =  htmlentities($this->__params['POST']['album'], ENT_QUOTES);
			$code =  htmlentities($this->__params['POST']['code'], ENT_QUOTES);

			$idStudent = $this->__db->execute("SELECT id FROM student WHERE email='".$email."' AND nr_index ='".$album."' LIMIT 1");

			$res=Null;
			if($idStudent!=Null){
				$idStudent=$idStudent[0];
				$res = $this->__db->execute("SELECT id_student
				FROM reservation
				WHERE url_key='".$url_key."' AND access_code='".$code."' AND id_student='".$idStudent['id']."' LIMIT 1");
			}
			else{
				$res=$res[0];
			}
			

			if($res==Null || $idStudent==Null){
				$alert=$this->getAlert(0, " Nie poprawne dane", "Wprowadz poprawne dane, takie jakie zostały wysłane na emaila");
				echo $alert;
			}
			else{
				$now = time();
				$expiryTime = 1800;
				$_SESSION['initiated'] = true;
				$_SESSION['idStudent'] = $idStudent['id'];
				if (isset($_SESSION['initiated']) && isset($_SESSION['idStudent']))
				{
					session_regenerate_id();
					$_SESSION['lastTrace'] = $now;
					$_SESSION['initiated'] = true;
					$_SESSION['idStudent'] = $idStudent['id'];
					$_SESSION['sendEmail'] = false;
					$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
				}
				header("Location:");
			}
		}
		if (!isset($_SESSION['initiated']) && !isset($_SESSION['idStudent']))
		{
			$this->getFormLoginStudent();
		}
		else if ((int)$_SESSION['lastTrace'] + $expiryTime < $now){
			$sessionName = session_name();    
			$_SESSION = array();
			if (isset($_COOKIE[$sessionName]))
			{
				setcookie($sessionName, '', $now-3600, '/');
			}
			session_destroy();
			header("Location:");
		}
		else{
			if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']){
				session_destroy();
				header("Location:");
			}
			else if($_SESSION['idStudent'] != $res['id_student']){
				$this->getFormLoginStudent();
			}
			else{
				$this->getDataStudent($idThesis);
			}
		}
	}

	public function getDataStudent($idThesis){
			$url_key = htmlentities($this->__params[1], ENT_QUOTES);
			
			$idStudent = $this->__db->execute("SELECT id_student
			FROM reservation
			WHERE url_key='".$url_key."' LIMIT 1")[0]['id_student'];

			if($idStudent==Null || $_SESSION['idStudent']!=$idStudent){
				$this->goToErorrPage();
			}
			
			$today = date("Y-m-d H:i:s"); 
			
			$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status 
			FROM thesis
			LEFT JOIN year on year.id = thesis.id_year 
			LEFT JOIN promoter on promoter.id = thesis.id_promoter
			LEFT JOIN reservation on reservation.id = thesis.id_reservation
			WHERE thesis.id='".$idThesis."'LIMIT 1")[0];

			if($thesis==NULL){
				$this->goToErorrPage();
			}

			$student = $this->__db->execute("SELECT * FROM student WHERE id='".$idStudent."' LIMIT 1")[0];
			if(isset($this->__params['POST']['save']) && isset($this->__params['POST']['email'])){
				if($this->__params['POST']['save']==1){
					$name = htmlentities($this->__params['POST']['name'], ENT_QUOTES);
					$surname = htmlentities($this->__params['POST']['surname'], ENT_QUOTES);
					$year_of_study = htmlentities($this->__params['POST']['year_of_study'], ENT_QUOTES);				
					$level_studies = htmlentities($this->__params['POST']['level_studies'], ENT_QUOTES);
					$type_of_studies = htmlentities($this->__params['POST']['type_of_studies'], ENT_QUOTES);				
					$field_of_study = htmlentities($this->__params['POST']['field_of_study'], ENT_QUOTES);
					$specialty = htmlentities($this->__params['POST']['specialty'], ENT_QUOTES);
					$res = $this->__db->execute("UPDATE student SET name='{$name}', surname ='{$surname}', year_of_study ='{$year_of_study}', level_studies='{$level_studies}', type_of_studies='{$type_of_studies}', field_of_study='{$field_of_study}', specialty='{$specialty}'  WHERE id= '{$student['id']}'");

					$student = $this->__db->execute("SELECT * FROM student WHERE id= '".$student['id']."' LIMIT 1")[0];
				}
			}

			$URL=$this->getProjectCatalogPath().'thesis/pdf/'.$idThesis.'/'.$url_key;
			echo '						
				<div class="row">
					<div class="col-md-6">
						<form action="" method="POST">
						<fieldset> 
							<legend>Dane studenta</legend>
									<div class="form-group">
										<label>Email</label>
										<input type="email" name="email" class="form-control" style="pointer-events: none" readonly value="'.$student['email'].'">
									</div>
									<div class="form-group">
										<label>Numer albumu (legitymacji)</label>
										<input type="text" name="album" class="form-control" style="pointer-events: none" readonly value="'.$student['nr_index'].'">
									</div>
									<div class="form-group">
										<label>Imię</label>
										<input type="text" name="Imie" class="form-control" disabled value="'.$student['name'].'" >
									</div>
									<div class="form-group">
										<label>Nazwisko</label>
										<input type="text" name="Nazwisko" class="form-control" disabled value="'.$student['surname'].'" >
								</div>
								<div class="form-group w-50 float-left pr-sm-2">
									<label for="exampleSelect1">Rok studiów</label>
									<select class="form-control" id="exampleSelect1" disabled>
										<option>'.$student['year_of_study'].'</option>
									</select>
								</div>
								<div class="form-group w-50 float-right">
									<label for="exampleSelect1">Studia</label>
									<select class="form-control" id="exampleSelect1" disabled>
										<option>'.$student['level_studies'].'</option>
									</select>
								</div>
								<div class="form-group w-50 float-left pr-sm-2">
									<label for="exampleSelect1">Typ studiów</label>
									<select class="form-control" id="exampleSelect1" disabled>
										<option>'.$student['type_of_studies'].'</option>
									</select>
								</div>
								<div class="form-group w-50 float-right">
									<label>Kierunek</label>
									<input type="text" name="field_of_study" class="form-control" disabled value="'.$student['field_of_study'].'">
								</div>
								<div class="form-group w-100 float-left m-0">
								<label>Specjaność</label>
								<input type="text" name="specialty" class="form-control" disabled value="'.$student['specialty'].'">
							</div>
						</fieldset>			
					</form>
				</div>';
			echo '	<div class="col-md-6">
						<div class="mb-4">		
							<h5 class="mt-2 mt-md-0">Wybrany temat pracy dyplomowej</h5>
							<div class="table-responsive">
								<table class="table-light text-dark w-100 text-justify">
										<tr class="table-primary">
											<td class="col-2">Promotor:</td>
											<td class="col-10">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</td>
										</tr>
										<tr>
											<td colspan="2">'.$thesis['topic'].'</td>
										</tr>
								</table>
							</div>
							<div class="text-center">
								<form action="'.$this->getProjectCatalogPath().'thesis/del_res" method="POST">
									<input type="hidden" name="thesis" value="'.$idThesis.'">
									<input type="hidden" name="url_key" value="'.$url_key.'">
									<button type="submit" class="btn btn-danger p-2 mt-3 mb-3">REZYGNUJ Z TEMATU</button>
								</form>
							</div>
						</div>';
						
						echo $this->getAlert(-1, "Uwaga!", "Deklarację należy pobrać i wydrukować.<br /> Podpisaną deklarację przez studenta i promotora należy dostyczyć do sekretariatu.");
							echo'

						<div class="row">
							<div class="col-md-6 float-left text-md-left text-center">
							<a href ='.$URL.' target="_blank" class="btn btn-info p-2 mt-2 mb-2">ZOBACZ DEKLARACJĘ</a> 
							</div>
							<div class="col-md-6 float-right text-md-right  text-center">
								<a href ="'.$URL.'/save" class="btn btn-info  p-2 mt-2 mb-2 ">POBIERZ DEKLARACJĘ</a>
							</div>
						</div>	
					</div>
				</div>';

				if($thesis['status']!="zarezerwowany"){
					echo '
					<div class="row pt-sm-3">
						<div class="col-6 float-left">	
							<form action="'.$this->getProjectCatalogPath().'thesis/edit/'.$idThesis.'/'.$url_key.'" method="POST">
								<button type="submit" class="btn btn-success float-left p-2 mt-2 mb-2">EDYTUJ DANE</button>
							</form>
						</div>
						<div class="col-6 float-right">
												
							<div class="text-right">
								<a href ="'.$this->getProjectCatalogPath().'logout"  class="btn btn-warning p-2 mt-2 mb-2">WYLOGUJ</a> 
							</div>
						</div>
					</div>';
				}
				echo'
				<div class="mb-2 mb-sm-0">
					<div class="row">
						<div class="progress m-auto border border-secondary"  style="min-width:150px; width: 25%;">
							<div class="progress-bar bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"><p class="m-0 p-0">Krok 2 / 2</p></div>
						</div>					
					</div>
				</div>';
	}
			
	public function getFormLoginStudent(){
		echo '	
			<form action="" method="POST">
				<fieldset> 
					<legend>Dane studenta</legend>
					<div class="row">
						<div class="col-md-6">	
								<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" placeholder="Enter email" required>
							</div>
							<div class="form-group">
								<label>Numer albumu (legitymacji)</label>
								<input type="text" name="album" class="form-control" pattern="\d*"  placeholder="Numer albumu" required>
							</div>	
							<div class="form-group">
								<label>Podaj hasło</label>
								<input type="password" name="code" class="form-control" placeholder="kod dostepu" required>
							</div>
						</div>
						<div class="col-md-6">';
						echo $this->getAlert(-1, "Weryfikacja dostępu!", "Dane potrzebne do logowania są w widomosci email");
							echo'
						</div>
					</div>';
				echo'
					<button type="submit" class="btn btn-success float-right p-2 mt-2 mb-2">ZALOGUJ</button>
				</fieldset>
			</form>';
	}
}

?>