<?php

class yearmodel extends generalmodel
{
	
	public function getYears($id_year)
	{
		$year = $this->__db->execute("SELECT * FROM year  
		WHERE id='".$id_year."' LIMIT 1")[0];
		return $year;
	}

	public function newYears()
	{
		if(isset($this->__params['POST']['years'])){
			$years=htmlentities($this->__params['POST']['years'], ENT_QUOTES);
			$access_code=bin2hex(random_bytes (4));
			$res = $this->__db->execute("INSERT INTO year VALUES (NULL,'{$years}','licencjackie','{$access_code}', 0 )");
			$access_code=bin2hex(random_bytes (4));
			$res = $this->__db->execute("INSERT INTO year VALUES (NULL,'{$years}','magisterskie','{$access_code}', 0 )");
			
		}
		header("Location: ".SERVER_ADDRESS."administrator/year");
	}

	public function getYear(){
		if(isset($this->__params[0]) && !empty($this->__params[0])){
			$id_years='';
			$id_years=htmlentities($this->__params[0], ENT_QUOTES);

			if($id_years=="new"){
				$this->newYears();
			}
			if(isset($this->__params['POST']['visible'])) {
				$visible=htmlentities($this->__params['POST']['visible'], ENT_QUOTES);
				if($visible==1 || $visible==0){
					$res = $this->__db->execute("UPDATE year SET visible='{$visible}'  WHERE id= '{$id_years}'");
				}
			}
			$this->getYearThesis($id_years);
		}
		else{
			$this->getYearList();
		}
	}

	public function getYearThesis($id_years){
			$year=$this->getYears($id_years);

			echo '
			<form class="form-inline " action="" method="POST">
			<div class="table-responsive">
			<table class="table-light text-dark ">';
			echo'
			<tr class="table-primary text-center">
				<td class="col-md-1">Rok akademicki</td>
				<td class="col-md-1">Studia</td>
				<td class="col-md-2">Kod dostepu dla promotorów</td>
				<td class="col-md-4">Adres dodawania tematów</td>
				<td class="col-md-1">Widoczny dla studentów</td>
			</tr>';

			$URL=$this->getProjectCatalogPath().'thesis/add/'.$year['id'];
				echo'
					<tr class="text-center">
						<td class="col-md-1">'.$year['years'].'</td>
						<td class="col-md-1">'.$year['type_of_studies'].'</td>
						<td class="col-md-2"><h4 class="p-0 m-0">'.$year['access_code'].'</h4></td>
						<td class="col-md-4"><a href="'.$URL.'">'.$_SERVER['SERVER_NAME'].$URL.'</a></td>
						<td class="col-md-1">
						<select class="form-control bg-secondary pt-2 pb-2 pl-3" name="visible"> 
						';

						if ($year['visible']==0){
							$visible="Nie";
							echo '							
							<option value="1">Tak</option>
							<option value="0" selected="selected" >Nie</option>';
						}
						else{
							echo '							
							<option value="1" selected="selected">Tak</option>
							<option value="0">Nie</option>';
						}
						
						echo '</select></td>
					</tr>
				</table>
			</div>';
			
			echo'
			<div class="row col-12 pt-sm-3 p-0 m-0">
				<div class="col-6 float-left p-0 m-0">';
					echo '<a href="administrator/year" class="btn btn-info p-2 mt-2 mb-2" role="button" >WRÓĆ DO ROCZNIKÓW</a>';
				echo'
				</div>
				<div class="col-6 float-right p-0 m-0">
					<button class="btn btn-success p-2 mt-2 mb-2 float-right" type="submit">ZAPISZ</button>
				</div>
            </form>
			</div>';
			
			echo'
			<h4 class="mt-3 mb-3">Przykładowa treść wiadomość (dla promotorów)</h4>
			<div class="alert alert-dismissible alert-info">
				<p>Możliwość zgłaszania tematów pracy dypomowej znajduje się pod adresem: <a href="'.$URL.'">'.$_SERVER['SERVER_NAME'].$URL.'</a><p>
				<p>Przeznaczone dla studiów: <strong class="font-weight-bold">'.$year['type_of_studies'].'</strong></p>
				<p>Klucz dostepu: <strong class="font-weight-bold">'.$year['access_code'].'</strong></p>
				<br>
				<p>Wiadmość jest przeznaczona wyłacznie dla promotów (nie należy jej udostepniać studentom)</p>
			</div>';
	}

	public function getYearList(){
		$years = $this->__db->execute("SELECT year.id, year.years, year.type_of_studies,access_code,visible FROM year 
		ORDER BY years DESC, type_of_studies");

		echo '
		<div class="row">
			<div class="col-md-8 pb-3">
				<div class="table-responsive">
				<table class="table-light text-dark">';
					echo'
					<tr class="table-primary text-center">
						<td class="col-md-2">Rok akademicki</td>
						<td class="col-md-2">Studia</td>
						<td class="col-md-2">Widoczny dla studentów</td>
					</tr>';
					foreach($years as $year)
					{
							echo'
								<tr class="text-center">
									<td class="col-md-2"><a href="administrator/year/'.$year['id'].'">'.$year['years'].'</a></td>
									<td class="col-md-2">'.$year['type_of_studies'].'</td>
									<td class="col-md-2">';

									if ($year['visible']==0){
										$visible="Nie";
									}
									else{
										$visible="Tak";
									}
									
									echo $visible.'</td>
								</tr>';
					}
				echo '</table>
				</div>
			</div>
			<div class="col-md-4 text-center">
				<h3 class="text-uppercase">Aktualny rok akademicki</h3>';
				$today = date("Y").'/'.(date("Y")+1);
				echo'<h3 class="text-uppercase">'.$today.'</h3>';
				$year = $this->__db->execute("SELECT * FROM year WHERE years ='".$today."' LIMIT 1");
				if(isset($year) && empty($year)){
					echo '
					<form class="text-center" action="administrator/year/new" method="POST">
						<input type="hidden" name="years" value="'.$today.'">
						<button type="submit" class="btn btn-success  p-2 mt-2 mb-2">DODAJ OBECNY ROK AKADEMICKI</button>
					</form>';
				}
			echo '
			</div>
		</div>';
	}
}
?>
