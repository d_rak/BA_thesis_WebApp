<?php

class showmodel extends generalmodel
{

	public function getYearsThesis($type)
	{
		$years = $this->__db->execute("SELECT year.id, year.years FROM year WHERE type_of_studies='".$type."' 
		ORDER BY years DESC");
		return $years;

	}

	public function getThesis($idYear,$typeThesis)
	{
		$theses = $this->__db->execute("SELECT thesis.id,id_promoter,topic, years,type_of_studies,qualification,name,surname,status FROM thesis 
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE id_year='".$idYear."' AND type_of_studies='".$typeThesis."'
		ORDER BY surname, name ");
		return $theses;
	}

	public function getTopicThesis($idThesis){ 
		$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,scientific_aspect,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status,id_student FROM thesis
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE thesis.id='".$idThesis."'LIMIT 1")[0];
		echo '<h3 class="text-dark m-2 mb-sm-4 text-center text-uppercase">Rok akademicki '.$thesis['years'].'</h3>';

		if($thesis==NULL){
			$this->goToErorrPage();
		}
		$typeThesis='';
		if(isset($this->__params[0]) && !empty($this->__params[0]))
		{
			$typeThesis=$this->getTypeThesis(2);
		}	
		echo '
		<div class="table-responsive ">
			<table class="table-light text-dark">
					<tr class="table-primary">
						<td class="col-2 col-md-3 p-md-4">Promotor:</td>
						<td class="col-10 col-md-9 p-md-4">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Temat pracy dyplomowej <br /> (j. polski):</td>
						<td class="p-md-4">'.$thesis['topic'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Temat pracy dyplomowej <br /> (j. angielski):</td>
						<td class="p-md-4">'.$thesis['topic_in_english'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Zakres pracy i oczekiwane rezultaty praktyczne:</td>
						<td class="p-md-4">'.$thesis['scope_of_work'].'</td>
					</tr>';

					if($typeThesis=="ma") {
						echo '		
						<tr>
							<td class="p-md-4">Aspekt naukowy, problemowy, innowacyjny pracy:</td>
							<td class="p-md-4">'.$thesis['scientific_aspect'].'</td>
						</tr>';
					}
					echo '
					<tr>
						<td class="p-md-4">Wymagane oprogramowanie/ języki programowania:</td>
						<td class="p-md-4">'.$thesis['required_software'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Środowisko uruchomieniowe:</td>
						<td class="p-md-4">'.$thesis['runtime'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Dodatkowe wymagania i uwagi:</td>
						<td class="p-md-4">'.$thesis['additional_requirements'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Literatura:</td>
						<td class="p-md-4">'.$thesis['literature'].'</td>
					</tr>
					<tr>
						<td class="p-md-4">Status tematu:</td>
						<td class="p-md-4 text-uppercase">'.$thesis['status'].'</td>
					</tr>
			</table>
			</div>';

			if($thesis['status']!='dostępny'){
				$student = $this->__db->execute("SELECT * FROM student WHERE id ='".$thesis['id_student']."' LIMIT 1")[0];
				echo '
				<h3 class="text-uppercase mt-5 mb-2 ">Rezerwacja - Dane studenta</h3>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="table-responsive">
							<table class="table-light text-dark w-100">
									<tr>
										<td class="col-4 p-md-4">Nazwisko i imię:</td>
										<td class="col-8 p-md-9">'.$student['name'].' '.$student['surname'].'</td>
									</tr>
									<tr>
										<td class="p-md-4">Nr albumu:</td>
										<td class="p-md-8">'.$student['nr_index'].'</td>
									</tr>
									<tr>
										<td class="p-md-4">Rok studiów:</td>
										<td class="p-md-8">'.$student['year_of_study'].'</td>
									</tr>
							</table>
						</div>
					</div>
					<div class="col-12 col-md-6 pt-3 pt-md-0">
						<div class="table-responsive">
							<table class="table-light text-dark w-100">
									<tr>
										<td class="col-4 p-md-4">Kierunek: <br/>Secjalność:</td>
										<td class="col-8 p-md-4">'.$student['field_of_study'].'<br/>'.$student['specialty'].'</td>
									</tr>
									<tr>
										<td class="p-md-4">Forma kształcenia:</td>
										<td class="p-md-4">'.$student['level_studies'].', '.$student['type_of_studies'].'</td>
									</tr>
							</table>
						</div>
					</div>
				</div>
				';

			}
			echo'
			<div class="row pt-sm-3">
				<div class="col-6 float-left">';
					$this->getReturnButton("WRÓĆ DO TEMATÓW");
				echo'
				</div>
				<div class="col-6 float-right">
					<form class="float-right" action="administrator/del" method="POST">
						<input type="hidden" name="thesis" value="'.$idThesis.'">
						<button type="submit" class="btn btn-danger  p-2 mt-2 mb-2">USUN TEMAT</button>
					</form>
				</div>
			</div>';
	}

	public function getThesisData(){	
		$shortTypeThesis=$this->getTypeThesis(2);
		if(isset($this->__params[1]) && !empty($this->__params[1])){
			$idThesis=htmlentities($this->__params[1], ENT_QUOTES);
			$this->getTopicThesis($idThesis);
		}
		else{
			$this->getThesesList();
		}
	}

	public function getThesesList(){
		$shortTypeThesis=$this->getTypeThesis(2);
		$typeThesis=$this->getTypeThesis(0);
		$years=$this->getYearsThesis($typeThesis);
		$i=0;
		foreach($years as $year)
		{
			if($i==0){
				echo '<h3 class="text-dark m-2 text-center text-uppercase">Rok akademicki '.$year['years'].' <br /> Tematy prac '.$this->getTypeThesis(1).'</h3>';
				$i++; 
			}
			else{
				echo '<h3 class="text-dark m-2 mb-md-3 mt-md-5 text-center text-uppercase">Rok akademicki '.$year['years'].' <br /> Tematy prac '.$this->getTypeThesis(1).'</h3>'; 
			}
			
			$theses =  $this->getThesis($year['id'], $typeThesis);     

			$promoter='';
			foreach($theses as $thesis){
				if($promoter!=$thesis['id_promoter']){
					$promoter=$thesis['id_promoter'];
					echo '
					<div class="row table-primary text-left m-0">
						<div class="col">
							<p class="m-sm-4 m-2 ">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</p>
						</div>
					</div>';
				}
				echo'
				<div class="row table-light p-md-3 p-2 m-0 border border-top-0">
					<div class="col-12 col-sm-12 col-md-5 col-lg-7 align-self-center text-justify">
						<a href="administrator/show/'.$shortTypeThesis.'/'.$thesis['id'].'" >'.$thesis['topic'].'</a>
					</div>
					<div class="col-12 col-sm-12 col-md-3 col-lg-3 text-center px-2 m-0 align-self-center">';
						if($thesis['status']=='dostępny'){
							$type_button='success';
						}
						else if($thesis['status']=='zarezerwowany'){
							$type_button='danger';
						}
						else{
							$type_button='warning';
						} 
						echo'<a href="" class="btn btn-'.$type_button.' disabled p-2 mt-2 mb-2" role="button" >'. $thesis['status'].'</a>
					</div> 
					<div class="col-12 col-sm-12 col-md-3 col-lg-2 text-center px-2 align-self-center">';
					if($thesis['status']=='wstępnie zarezerwowany'){
						echo '<a href="administrator/accept/'.$shortTypeThesis.'/'.$thesis['id'].'" class="btn btn-success p-2 mt-2 mb-2" role="button" >ZATWIERDŹ</a>';
					}
					echo'
					</div>
				</div>';
			}
		}
	}
}
?>