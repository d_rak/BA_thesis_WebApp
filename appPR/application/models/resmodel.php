<?php

class resmodel extends generalmodel
{

	public function getTopicThesis(){

		$typeThesis=$this->getTypeThesis(2);
		if(isset($this->__params['POST']['email']) && isset($this->__params['POST']['album']) && isset($this->__params['POST']['url_key']) )
		{
			$email = htmlentities($this->__params['POST']['email'], ENT_QUOTES);
			$album = htmlentities($this->__params['POST']['album'], ENT_QUOTES);

			$student = $this->__db->execute("SELECT id FROM student WHERE nr_index ='".$album."' LIMIT 1");
			if($student!=Null || strlen($album)<6){
				$error=1;
				$alert=$this->getAlert(0, "Błędny numer albumu", "Numer indeksu jest niepoprawny lub zarejestrowany");
				echo $alert;
			}
			else{
				$url_key = htmlentities($this->__params['POST']['url_key'], ENT_QUOTES);
				
				$idThesis=$this->getIdThesis(1);
				$id_res = $this->__db->execute("SELECT id_reservation
				FROM thesis
				WHERE id='".$idThesis."'LIMIT 1")[0];

				$res = $this->__db->execute("INSERT INTO student VALUES (NULL,NULL,NULL,'{$email}','{$album}',NULL,NULL,NULL,NULL,NULL)");
				$idStudent = $this->__db->execute("SELECT id FROM student WHERE email='".$email."' AND nr_index ='".$album."' LIMIT 1")[0];
				
				$today = date("Y-m-d H:i:s"); 

				$id_res = $this->__db->execute("SELECT id_reservation
				FROM thesis
				WHERE id='".$idThesis."' LIMIT 1")[0];

				$access_code= $this->__db->execute("SELECT access_code
				FROM reservation
				WHERE id= '{$id_res['id_reservation']}'")[0];

				if($access_code['access_code']== NULL){
					$access_code=bin2hex(random_bytes (4));
					$res = $this->__db->execute("UPDATE reservation SET id_student='{$idStudent['id']}', status = 'wstępnie zarezerwowany', date ='{$today}', url_key='{$url_key}', access_code = '{$access_code}'  WHERE id= '{$id_res['id_reservation']}'");
				}

				$now = time();
				$expiryTime = 1800;
				$_SESSION['initiated'] = true;
				$_SESSION['idStudent'] = $idStudent['id'];
				if (isset($_SESSION['initiated']) && isset($_SESSION['idStudent']))
				{
					session_regenerate_id();
					$_SESSION['lastTrace'] = $now;
					$_SESSION['initiated'] = true;
					$_SESSION['idStudent'] = $idStudent['id'];
					$_SESSION['sendEmail'] = true;
					$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
				}
				header("Location:".$this->getProjectCatalogPath().'thesis/edit/'.$idThesis.'/'.$url_key);
			}
		}

		$idThesis=$this->getIdThesis(1);
		$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status FROM thesis
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE thesis.id='".$idThesis."'LIMIT 1")[0];
		$url_key=(bin2hex(random_bytes (5)));
		
		if($thesis==NULL){
			$this->goToErorrPage();
		}
		
		echo '						
		<form action="'.$this->getProjectCatalogPath().'thesis/res/'.$typeThesis.'/'.$idThesis.'/" method="POST">
			<fieldset> 
				<legend>Dane studenta</legend>
				<div class="row">
					<div class="col-md-6">
								<div class="form-group">
									<label>Email</label>
									<input type="email" name="email" class="form-control" placeholder="Wprowadź email" required value="';
									if (!empty($this->__params['POST']['email'])) echo htmlentities($this->__params['POST']['email'], ENT_QUOTES);
									echo '">

								</div>
								<div class="form-group">
									<label>Numer albumu (legitymacji)</label>
									<input type="text" name="album" pattern="\d*" class="form-control ';
									if($error==1){
										echo "is-invalid";
									}
									echo '" placeholder="Wprowadź numer albumu" required value="';
									if (!empty($this->__params['POST']['album'])) {
										echo htmlentities($this->__params['POST']['album'], ENT_QUOTES);
									}
									echo'">
								</div>
								<input type="hidden" name="message" value="1">
								<input type="hidden" name="url_key" value="'.$url_key.'">
					</div>
					<div class="col-md-6">
						<div class="alert alert-dismissible alert-warning">
							<h4 class="alert-heading">Uwaga!</h4>
							<p class="mb-0">Sprawdź poprawność pola email i numer albumu (tych danych nie da się później zmienić).</p>
							<br />
							<p class="mb-0">Email jest bardzo ważny, na niego zostanie wysłany link, pozwalajcy poźniej uzupelnić i edytować dane do dekalaracji, czy też zrezygnować z tematu.</p>
						</div>
					</div>
				</div>
				<h4 class="text-uppercase">Wybrany temat</h4>
				<div class="table-responsive">
					<table class="table-light text-dark w-100">
							<tr class="table-primary">
								<td class="col-3">Promotor:</td>
								<td class="col-9">'.$thesis['qualification'].' '.$thesis['name'].' '.$thesis['surname'].'</td>
							</tr>
							<tr>
								<td>Temat pracy dyplomowej <br /> (j. polski):</td>
								<td >'.$thesis['topic'].'</td>
							</tr>
					</table>
				</div>
				<div class="row pt-sm-3">
					<div class="col-6 float-left">';
						$this->getReturnButton("WRÓĆ DO TEMATÓW");
				echo'	
					</div>
					<div class="col-6 float-right">';		
						if($thesis['status']=='dostępny'){	
							$shortTypeThesis=$this->getTypeThesis(2);
							$type_button='success';
							echo'
							<button type="submit" class="btn btn-'.$type_button.' float-right p-2 mt-2 mb-2">POTWIERDZAM REZERWACJE</button>';
						}
				echo'</div>
				
				</fieldset>
			</form>';			
	}
}
?>