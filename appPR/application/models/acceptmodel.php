<?php

class acceptmodel  extends generalmodel
{
	public function setAccept(){
		$idThesis=$this->getIdThesis(1);

		if(isset($this->__params['POST']['accept']))
		{	
			$shortTypeThesis=$this->getTypeThesis(2);
			$idReserbation=htmlentities($this->__params['POST']['accept']);
			$res = $this->__db->execute("UPDATE reservation SET status='zarezerwowany' WHERE id= '{$idReserbation}'");
			$URL=$this->getProjectCatalogPath()."administrator/show/".	$shortTypeThesis;
			header("Location:  ".$URL); 
		}

		$thesis = $this->__db->execute("SELECT thesis.id,topic,topic_in_english,scope_of_work,required_software,runtime,additional_requirements,literature,years,type_of_studies,qualification,name,surname,status,id_student,id_reservation FROM thesis
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE thesis.id='".$idThesis."'LIMIT 1")[0];
		$student = $this->__db->execute("SELECT * FROM student WHERE id ='".$thesis['id_student']."' LIMIT 1")[0];
		echo '						
			<div class="row mb-3">
				<div class="col-md-6">
					<h3 class="text-uppercase">Temat pracy dyplomowej</h3>
					<div class="table-responsive">
						<table class="table-light text-dark w-100">
								<tr class="table-primary">
									<td class="col-2">Promotor:</td>
									<td class="col-10">'.$thesis['qualification'].' '.$thesis['surname'].' '.$thesis['name'].'</td>
								</tr>
								<tr>
									<td colspan=2>'.$thesis['topic'].'</td>
								</tr>
						</table>
					</div>
				</div>
				<div class="col-md-6">
					<h3 class="text-uppercase">Dane studenta</h3>
					<div class="table-responsive">
						<table class="table-light text-dark w-100">
								<tr>
									<td class="col-4">Nazwisko i imię:</td>
									<td class="col-8">'.$student['surname'].' '.$student['name'].'</td>
								</tr>
								<tr>
									<td>Nr albumu:</td>
									<td>'.$student['nr_index'].'</td>
								</tr>
								<tr>
									<td>Rok studiów:</td>
									<td>'.$student['year_of_study'].'</td>
								</tr>
								<tr>
								<td>Kierunek: <br/>Secjalność:</td>
								<td >'.$student['field_of_study'].'<br/>'.$student['specialty'].'</td>
							</tr>
								<tr>
									<td>Forma kształcenia:</td>
									<td>'.$student['level_studies'].', '.$student['type_of_studies'].'</td>
								</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="row pt-sm-3">
				<div class="col-6 float-left">';
					$this->getReturnButton("WRÓĆ DO TEMATÓW");
				echo'
				</div>
				<div class="col-6 float-right">
				<form action="" method="POST" class="float-right">
					<input type="hidden" name="accept" value="'.$thesis['id_reservation'].'">
					<button type="submit" class="btn btn-success  p-2 mt-2 mb-2">POTWIERDZAM DEKLARACJE</button>
				</form>	
				</div>
			</div>';

	}
	
}



?>