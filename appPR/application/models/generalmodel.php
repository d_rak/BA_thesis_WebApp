<?php

class generalmodel
{
    protected $__config;
    protected $__router;
    protected $__db;
	protected $__params;
    
    public function __construct()
    {
        $this->__config = registry::register("config");
        $this->__router = registry::register("router");
        $this->__db = registry::register("db");
		$this->__params = $this->__router->getParams();
	}

	public function getYearsThesis($type)
	{
		$years = $this->__db->execute("SELECT year.id, year.years FROM year WHERE type_of_studies='".$type."' AND visible=1
		ORDER BY years DESC");
		return $years;

	}

	public function goToErorrPage()
	{
		header("Location:".SERVER_ADDRESS);
	}

	public function getProjectCatalogPath()
	{
		return $this->__config->__get("project_catalog_path");

	}

	public function getTypeThesis($nr)		//0- jakie tematy //1-jakich tematów //2 - angielski skrot
	{
		$typeThesis='';
		if(isset($this->__params[0]) && !empty($this->__params[0]))
		{
			
			$typeThesis=htmlentities($this->__params[0], ENT_QUOTES);
			
			if($typeThesis=='ba')
			{
				$studies=['licencjackie','licencjackich', 'ba'];
				return $studies[$nr];
			}
			elseif($typeThesis=='ma')
			{
				$studies=['magisterskie','magisterskich', 'ma'];
				return $studies[$nr];
			}
			else{
				$this->goToErorrPage();
				echo $this->sgException->errorPage(404);
			}	
		}else{
			$this->goToErorrPage();
		}	
	}

	
	public function getReturnButton($buttonContent)	
	{
		if(isset($_SERVER['HTTP_REFERER'])) {
			echo '<a href ='.$_SERVER["HTTP_REFERER"].' class="btn btn-info  p-2 mt-2 mb-2">'.$buttonContent.'</a> ';
		}
		else{
			$url=$_SERVER['REQUEST_URI'];
			$url=substr($url, 0, strrpos($url, "/"));
			echo '<a href ='.$url.' class="btn btn-info  p-2 mt-2 mb-2">'.$buttonContent.'</a> '; 
		}

	}

	public function getTheses($idYear,$typeThesis)
	{
		$theses = $this->__db->execute("SELECT thesis.id,id_promoter,topic, years,type_of_studies,qualification,name,surname,status FROM thesis 
		LEFT JOIN year on year.id = thesis.id_year 
		LEFT JOIN promoter on promoter.id = thesis.id_promoter
		LEFT JOIN reservation on reservation.id = thesis.id_reservation
		WHERE id_year='".$idYear."' AND type_of_studies='".$typeThesis."' AND visible = '1'
		ORDER BY surname, name ");
		return $theses;
	}

	public function getIdThesis($nr){ //$nr - parameter number 0 or 1 example ../id0/id1/..
		$idThesis='';
		if(isset($this->__params[$nr]) && !empty($this->__params[$nr]))
		{
			$idThesis=htmlentities($this->__params[$nr], ENT_QUOTES);
			return $idThesis;
		}
		return '';
	}

	public function getAlert($type,$title,$contents){	//1=succes 0-wrong -1-warring 2-info
		if($type==1){
			$typeAlests='success';
		}
		else if($type==0)
		{
			$typeAlests='danger';
		}
		else if($type==-1)
		{
			$typeAlests='warning';
		}
		else{
			$typeAlests='info';
		}
		
		$alert='
		<div class="alert alert-dismissible alert-'.$typeAlests.' px-3">
			<h4 class="alert-heading text-uppercase">'.$title.'</h4>
			<p class="mb-0 text-justify">'.$contents.' </p>
		</div>';
		return $alert;
	}
	
	public function logout()
	{
		header("Location:".SERVER_ADDRESS);
		
	}

	public function __call($method, $params)	
	{
		echo 'Cos innego ;)';
	}
}
?>