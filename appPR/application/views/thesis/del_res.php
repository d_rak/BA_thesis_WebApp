<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>System rejestracji i rezerwacji tematów prac dyplomowych - Tematy prac Licencjackich</title>
    <?=module_load('HEADLINKFILE')?>
    <?=add_basehref()?>
</head> 
<body class="bg-dark">
    <div class= "container-fluid bg-dark">
        <div class="container bg-dark ">    
            <?=module_load('HEADER')?>
        </div>
    </div>
    <div class= "container-fluid bg-dark "> 
        <div class="container">
        <?=module_load('MENU')?>
        </div>
    </div>
    <div id="content-fluid-bg" class= "content-fluid p-sm-4 p-2 pt-4" >
        <div class="container bg-light">
            <div id="content-header">
                <div class="bg-dark p-2 pl-3 pr-3">
                    <h5 class="text-light m-0 text-uppercase">Rezygnacja z tematu</h5>
                </div>  
            </div>
            <div class="content p-3 p-sm-3 p-lg-4">    
                <?=model_load("del_resmodel", "del_reservation", 1)?>
            </div>
        </div>
    </div>	
    <div class= "container-fluid bg-dark"> 
        <div class="container bg-dark p-3"> 
        <?=module_load('FOOTER')?>
        </div>
    </div>  
</body>
</html>