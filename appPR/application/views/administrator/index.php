<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>System rejestracji i rezerwacji tematów prac dyplomowych - Tematy prac Licencjackich</title>
    <?=module_load('HEADLINKFILE')?>
    <?=add_basehref()?>
</head> 

<body class="bg-dark">
    <div id="content-fluid-bg" class= "content-fluid p-4" >
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto bg-light ">
                    <div id="content-header">
                        <div class="bg-dark p-2 pl-3 pr-3">
                            <h5 class="text-light m-0">Panel Administarcyjny</h5>
                        </div>  
                    </div>
                    <div class="content p-3">
                        <h2 class="text-dark mb-3 text-center">LOGOWANIE</h2>
                        <form method="post" action="">
                            <?php 
                                $alert = model_load("administratormodel", "getAlert", "");
                                if($alert!=''){
                                    $alertType = model_load("administratormodel", "getAlertalertType", "");
                                    echo '<div class="alert p-0 m-0">
                                    <p class="alert-dismissible alert-'.$alertType.' p-3 ">'.$alert.'</p>
                                    </div>';
                                }
                            ?>    
                            <div class="form-group">
                                <label form="username" >Nazwa użytkownika </label> 
                                <input type="text" required class="form-control"  name="login" placeholder="Użytkownik" autofocus value="<?php echo model_load("administratormodel", "getLogin", "")?>"/>
                            </div>
                            <div class="form-group">
                                <label form="password">Hasło </label> 
                                <input type="password" required class="form-control " placeholder="Hasło" name="pass" value=""/>
                            </div>
                            <div class="text-right">
                                <input type="submit" class="btn btn-primary mt-3 mb-3 text-center submit" name="submit" value="wyślij" />   
                            </div>
                        </form>
                    </div>
                </div>
            </div>	
        </div>	
    </div>	
</body>
</html>