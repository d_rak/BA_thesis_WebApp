<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>System rejestracji i rezerwacji tematów prac dyplomowych</title>
    <?=module_load('HEADLINKFILE')?>
    <?=add_basehref()?>
</head> 

<body class="bg-dark">
    <div class= "container-fluid bg-dark">
        <div class="container bg-dark ">    
            <?=module_load('HEADER')?>
        </div>
    </div>
    <div class= "container-fluid bg-dark "> 
        <div class="container">
        <?=module_load('MENU')?>
        </div>
    </div>
    <div id="content-fluid-bg" class= "content-fluid p-4" >
        <div class="container bg-light text-dark">
            <div id="content-header">
                <div class="bg-dark p-2 pl-3 pr-3">
                    <h5 class="text-light m-0 text-uppercase">Tematy prac DYPLOMOWYCH</h5>
                </div>  
            </div>
            <div class="content p-3 p-sm-3 p-lg-4">              
                <p> Strona umożliwia znalezienie i zarezerwowanie tematu pracy dyplomowej.</p>
                <p> Tematy prac dyplomowych są podzielone względem toku studiów. Wybierz ten, który Cię insteresuje. </p>
                <div class="m-3 text-center">
                    <a href="thesis/view/ba" class="btn btn-info p-3 my-2 m-md-2" >Tematy prac LICENCJACKICH</a>
                    <a href="thesis/view/ma" class="btn btn-info p-3 my-2 m-md-2" >Tematy prac MAGISTERSKICH</a>
                </div> 
                <p> Procedurę wyboru tematu pracy dyplomowej znajdziesz w zakładce <a href="info" class="link"><u>informacje</u></a> warto też zajrzeć do zakładki <a href="info" class="link"><u>FAQ</u></a>.<p>
            </div>
        </div>
    </div>	
    <div class= "container-fluid bg-dark"> 
        <div class="container bg-dark p-3"> 
        <?=module_load('FOOTER')?>
        </div>
    </div>  
</body>
</html>