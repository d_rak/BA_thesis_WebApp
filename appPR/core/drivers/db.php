<?php

class db
{
	public $result = Array();
	private $connect;
	
	public function __construct($_host=false, $_user=false, $_pass=false, $_name=false, $_charset=false)
	{
		$config = registry::register("config");
		
		$host = $config->db_host;
        $user = $config->db_user;
        $pass = $config->db_pass;
		$name = $config->db_name;
		$charset = $config->db_charset;
		
		if(!isset($host))
		{
			$host = $_host;
            $user = $_user;
            $pass = $_pass;
			$name = $_name;
			$charset = $_charset;
		}
		
		if($host === false)
		{
			die('Nie przekazano dostępu do bazy.');
		}
		
		$this->connect = new mysqli($host, $user, $pass, $name);
		if(mysqli_connect_errno() !== 0)
		{
			throw new Exception("Błąd połączenia z bazą danych : ".mysqli_connect_error());
		}
		else
		{
			$this->connect->set_charset($charset);
			return $this->connect;
		}
	}
	
	public function execute($sql)
	{
		$query_arr = explode(" ", trim($sql));
		$query_type = strtoupper($query_arr[0]);
		
		if($query_type == 'SELECT' || $query_type == 'SHOW')
		{
			return $this->selectable($sql);
		}
		else if($query_type == 'UPDATE' || $query_type == 'DELETE' || $query_type == 'DROP' || $query_type == 'INSERT' || $query_type == 'ALTER' || $query_type == 'CREATE')
		{
			return $this->modifiable($sql);
		}
		
		return false;
	}
	
	private function selectable($query)
	{
		$this->result = array();
		
		$answer = $this->connect->query($query);
		if(!$answer)
		{
			return false;
		}
		else
		{
			while (($db_result = $answer->fetch_assoc()) !== null)
			{
				array_push($this->result, $db_result);
			}
			
			return $this->result;
		}
		
		$this->close();
	}
	
	private function modifiable($query)
	{
		$this->result = array();	
		$answer = $this->connect->query($query);
		return (!($answer)) ? false : true;
	}
	
	public function getRow()
	{
		$row = mysqli_fetch_row($this->result);
		return $row;
	}
	
	public function count()
	{
		$count = count($this->result);
		return (!$count || !is_int($count)) ? 0 : $count;
	}
	
	public function close()
	{
		if($this->connect) mysqli_close($this->connect);
	}
	
	public function __destruct()
	{
		$this->close();
	}
}

?>