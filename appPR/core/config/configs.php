<?php

if(!defined('DS')) define('DS', '/');

$AbsoluteURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$AbsoluteURL .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
$slash = substr($AbsoluteURL, -1);
$NewURL = $slash != '/' ? $AbsoluteURL.'/' : $AbsoluteURL;
define('SERVER_ADDRESS', $NewURL);

/**
 * USTAWIENIA SYSTEMU - WYŚWIETLANIE BŁEDÓW
 */
error_reporting(0);
// error_reporting(E_ALL);

/**
 * USTAWIENIA SYSTEMU - PODSTAWOWE
 */
$configs['default_controller'] = "home";

/**
 * USTAWIENIA SYSTEMU - ZAAWANSOWANE
 */
$configs['default_session_auth_var'] = 'login';
$configs['default_session_admin_auth_var'] = 'admin_login';

/**
 * USTAWIENIA SYSTEMU - ŚCIEŻKI KATALOGÓW
 */
$configs['images_catalog_name'] = "images";
$configs['javascript_catalog_name'] = "js";
$configs['stylesheet_catalog_name'] = "css";
$configs['fonts_catalog_name'] = "fonts";
		        					
//katalog instalacji projektu w ktorym jest cały project aplikacji
$configs['project_catalog_path'] = substr(SERVER_ADDRESS, strrpos($AbsoluteURL, "/")); 

$configs['controller_path'] = "application".DS."controllers".DS;
$configs['model_path'] = "application".DS."models".DS;
$configs['view_path'] = "application".DS."views".DS;
$configs['media_path'] = "application".DS."media".DS;
$configs['module_path'] = "application".DS."library".DS;

$configs['app_images_path'] = $configs['project_catalog_path']."application".DS."media".DS.$configs['images_catalog_name'].DS;
$configs['app_javascript_path'] = $configs['project_catalog_path']."application".DS."media".DS.$configs['javascript_catalog_name'].DS;
$configs['app_stylesheet_path'] = $configs['project_catalog_path']."application".DS."media".DS.$configs['stylesheet_catalog_name'].DS;

$configs['helper_path'] = "core".DS."helpers".DS;
$configs['library_path'] = "core".DS."library".DS;
$configs['driver_path'] = "core".DS."drivers".DS;

/**
 * USTAWIENIA SYSTEMU - BAZA DANYCH - dane testowe 
 */
$configs['db_host'] = "db-service";
$configs['db_user'] = "root";
$configs['db_pass'] = "secret";
$configs['db_name'] = "database_thesis";
$configs['db_charset'] = 'utf8';

/**
 * USTAWIENIA SYSTEMU - EMAIL - dane do ustawienia
 */
$configs['mail_host'] = '';             // Adres serwera SMTP
$configs['mail_SMTP_Auth']  = true;     // Autoryzacja (do) SMTP
$configs['mail_username'] =  "";        // Nazwa użytkownika
$configs['mail_password'] = "";         // Hasło
$configs['mail_SMTP_secure'] = 'tls';   // Typ szyfrowania (TLS/SSL)
$configs['mail_port'] = 587;            // Port
$configs['mail_charset'] = "UTF-8";

/**
 * Panel Administracyjny - dane testowe do logowania
 */
# URL: /administrator
# Login:sekretariat
# Haslo:Sekretariat!up@pl   //w bazie jest hash tego hasla dla testów podaje tutaj
?>