<?php
require 'phpoffice/vendor/autoload.php';
class phpoffice
{
    private $__router;
    private $__config;
    private $__params;
	
	public function __construct()
    {
        $this->__router = registry::register("router");
        $this->__config = registry::register("config");
        $this->__params = $this->__router->getParams();
    }

    public function LoadFileData($nameFile,$tmp_name)
	{
        try {
            $phpWord = \PhpOffice\PhpWord\IOFactory::load($tmp_name);
            $section = $phpWord->addSection();

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        
            $nameFile=preg_replace('(.docx)',"",$nameFile);

            $name_temp_file_html=realpath(dirname(__FILE__))."\\phpoffice\\temp\\".date('Y_m_d_H_m_s')."_".$nameFile.".html";
            $objWriter->save($name_temp_file_html);

            libxml_disable_entity_loader(false);
        
            $doc = new DOMDocument();
            $doc->loadHTMLFile($name_temp_file_html);
            
            $elem = $doc->getElementsByTagName('td');
            
            $data = [];
            foreach($elem  as $i => $table)
            {
                $text=$doc->saveXML($table);
                $text = strip_tags($text, '<p>');
                $text = preg_replace('( style=(".*;"))',"",$text);
                $text = preg_replace('(&#13;)',"",$text);
                $text = preg_replace('(\n)',"",$text);
                if($i%2==1)
                    array_push($data, $text);
            }
            if(!isset($data[0])){
                return 0;
            }
            $data[0]  = strip_tags($data[0], '');
            $uchwyt = fopen($name_temp_file_html, "w");
            unlink($name_temp_file_html);
            return $data;  

        } catch (Exception $e) {
            return 0;
        }
    }
}