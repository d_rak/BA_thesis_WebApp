<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class mailer
{
	private $__db;
    private $__router;
    private $__config;
	
	public function __construct()
    {
        $this->__router = registry::register("router");
        $this->__config = registry::register("config");
        $this->__db = registry::register("db");
    }


    public function MailTemplate($addresWebThesis,$access_code)
	{
        $mail='<html>
                <head>
                    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
                    <meta content="width=device-width" name="viewport"/>
                    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
                    <title>Rezeracja tematu</title>
                </head>
                <body style="margin: 0; padding: 0; font-family: Arial, Verdana; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
                    <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; width: 100%;" valign="top" width="100%">
                        <tbody>
                            <tr style="vertical-align: top;" valign="top">
                                <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
                                    <div style="background-color:transparent;">
                                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 800px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <h2 style="font-size: 18px; line-height: 20px; color: #555555; font-family: Arial,\'Helvetica Neue\', Helvetica, sans-serif; margin: 0;">Twoja rezerwacja tematu pracy dyplomowej jest potwierdzona</h2>
                                                </div>
                                                <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 12px; line-height: 14px; color: #555555;">Rezerwacja jest dostępna pod adresem <a href="'.$addresWebThesis.'" rel="noopener" style="text-decoration: underline; color: #0068A5;" target="_blank">'.$addresWebThesis.'</a> lub kliknij przycisk by odwiedzić stronę</div>
                                                </div>
                                                <div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <a href="'.$addresWebThesis.'" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #1F9BCF; width: auto; width: auto; border: 0; padding-top: 5px; padding-bottom: 5px; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; text-transform: uppercase; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank">
                                                        <span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;">
                                                            <span style="font-size: 16px; line-height: 32px;">Zobacz zarezerwowany temat</span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; font-size: 12px; line-height: 14px; color: #555555;">
                                                        <p style="font-size: 14px; line-height: 16px; color: #555555; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; margin: 0;">Twój kod dostępu: <strong>'.$access_code.'</strong></p>
                                                    </div>
                                                </div>
                                                <div style="color:#555555;font-family:Arial, \'Helvetica Neue\', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <p style="font-size: 12px; line-height: 14px; color: #555555; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; margin: 0;">Ta wiadomość jest ważna, nie należy jej usuwać i udostępniać. Przechodząc przez link (lub klikając przycisk) przechodzisz do panelu edycji, statusu rezerwacji, tam też możesz zrezygnować tematu.</p>
            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>';
    return $mail;  
    }




    public function MailGenerator($email,$addresWebThesis,$access_code)
	{   

        require 'phpmailer/src/Exception.php';
        require 'phpmailer/src/PHPMailer.php';
        require 'phpmailer/src/SMTP.php';
        
        $tresc = $this->MailTemplate($addresWebThesis,$access_code);
        $temat = 'Rezerwacja tematu pracy dyplomowej';
        date_default_timezone_set('Europe/Warsaw');
        //$email='przykladowy@gmail.com';                       // przydatne do testów, by wysylać na swoj mail       

        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();                                       // Używamy SMTP

            $mail->Host = $this->__config->mail_host;              // Adres serwera SMTP
            $mail->SMTPAuth = $this->__config->mail_SMTP_Auth;     // Autoryzacja (do) SMTP
            $mail->Username = $this->__config->mail_username;      // Nazwa użytkownika
            $mail->Password = $this->__config->mail_password;      // Hasło
            $mail->SMTPSecure = $this->__config->mail_SMTP_secure; // Typ szyfrowania (TLS/SSL)
            $mail->Port =$this->__config->mail_port;
            $mail->CharSet = $this->__config->mail_charset;
            $mail->setLanguage('pl', '/phpmailer/language');

            $mail->setFrom($mail->Username, 'System rezerwacji tematów');
            $mail->addAddress($email, '');
            //$mail->addReplyTo($email, 'Test');        //kopia emaila

            $mail->isHTML(true); 
            $mail->Subject = $temat;
            $mail->Body = $tresc;
            $mail->AltBody = 'By wyświetlić wiadomość należy skorzystać z czytnika obsługującego wiadomości w formie HTML';

            $mail->send();
            return 1;   
        
        } catch (Exception $e) {
            return 0;
        }

    }
}