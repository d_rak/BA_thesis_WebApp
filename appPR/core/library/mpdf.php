<?php

class mpdf
{
	private $__db;
    private $__router;
    private $__config;
    private $__params;
	
	public function __construct()
    {
        $this->__router = registry::register("router");
        $this->__config = registry::register("config");
        $this->__db = registry::register("db");
        $this->__params = $this->__router->getParams();
    }

    public function PdfTemplateBody($idThesis,$idStudent)
	{

        $Thesis = $this->__db->execute("SELECT id_reservation,id_promoter,topic  FROM thesis WHERE id ='".$idThesis."' LIMIT 1")[0];
        $student = $this->__db->execute("SELECT * FROM student WHERE id ='".$idStudent."' LIMIT 1")[0];
        $promoter = $this->__db->execute("SELECT * FROM promoter WHERE id ='".$Thesis['id_promoter']."' LIMIT 1")[0];

        if($Thesis==NULL || $student==NULL){
			header("Location:".SERVER_ADDRESS);
		}
        $html = ' 
        <html lang="pl-PL"> 
            <head>                    
                <meta charset="utf-8">
                <style>
                <title>Deklaracja</title>	
                    * {
                        font-family: Arial, Verdana;
                        margin:0;
                        padding:0;
                    }
                    h1,h2,h3,p{
                        font-family: Arial, Verdana;
                    }
                    
                    
                    header {
                        font-size:10pt;
                        color: grey;
                        text-align: center;
                        line-height: 0.6cm;				
                        padding-bottom: -0.25cm;
                        border-bottom:1px solid black;
                    }
                    
                    table {
                        font-family: Arial, Verdana;
                        font-size:10pt;
                        line-height: 0.7cm;	
                    }
                </style>
            </head>
            <body>
                <header>
                    <div>
                        <p>Uniwersytet Pedagogiczny w Krakowie<br>
                        <b style="color:black">Instytut Informatyki</b><br>
                        ul. Podchorążych 2, 30 -084 Kraków, tel. 12 -662-78-45, fax. 12-662-61-66</p>
                    </div>
                </header>
                <main>
                    <h2 style="text-align:center; margin:1cm 0; text-transform: uppercase;">Deklaracja wyboru tematu pracy dyplomowej</h2>
                    <table>
                        <tbody>
                            <tr>
                                <td style="width:40%;">Nazwisko i imię:</td>
                                <td>'.$student['surname'].' '.$student['name'].'</td>
                            </tr>
                            <tr>
                                <td>Nr albumu:</td>
                                <td>'.$student['nr_index'].'</td>
                            </tr>
                            <tr>
                                <td>Rok studiów:</td>
                                <td>'.$student['year_of_study'].'</td>
                            </tr>
                            <tr>
                                <td>Kierunek/specjalność:</td>
                                <td>'.$student['field_of_study'].', '.$student['specialty'].'</td>
                            </tr>
                            <tr>
                                <td>Forma kształcenia:</td>
                                <td>'.$student['level_studies'].', '.$student['type_of_studies'].'</td>
                            </tr>
                        </tbody>
                    </table>
                    <div style=" margin:1cm 0; ">
                        <h2 style="text-align:center; margin:1cm 0;">OŚWIADCZENIE</h2>	
                        <p>Oświadczam, że wybrany przeze mnie temat pracy dyplomowej jest następujący:</p>
                        <p style="font-size:12pt; line-height: 0.8cm; text-align: justify;"><em>"'.$Thesis['topic'].'".</em></p>
                        <p style="text-align:right; margin:0.5cm 0;">Promotorem pracy jest:</p>
                        <p style="text-align:right; margin:0.5cm 0;">'.$promoter['qualification'].' '.$promoter['name'].' '.$promoter['surname'].'</p>
                    </div>			
                    <div style=" margin:1cm 0; width:100%;">
                        <div style="float:left; width:30%;">
                            <p style="margin:1cm 0; text-align:center;">Data i podpis studenta:</p>
                            <p style="text-align:center;">.......................................<p>
                        </div>
                        <div style="float:right; width:30%;">
                            <p style="margin:1cm 0; text-align:center;">Data i podpis Promotora:</p>
                            <p style="text-align:center;">.......................................<p>
                        </div>
                    </div>
                </main>

            </body>
        </html>
        ';
    return $html;  
    }

    public function PdfTemplateFooter()
	{
        $html = ' 
        <table style="width:100%; color:grey">
            <tr>
                <td style="width:33%">'.date("d-m-Y H:i").'</td>
                <td style="width:33; text-align: center"></td>
                <td style="width:33%; text-align: right;"></td>
            </tr>
        </table>';
    return $html;  
    }


    public function PdfGenerator($idThesis,$idStudent)
	{   
    require_once("mpdf/vendor/autoload.php"); 
    var_dump(require_once("mpdf/vendor/autoload.php"));
    try {

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'P',
        ]);

        $html=$this->PdfTemplateBody($idThesis,$idStudent);
        $mpdf->SetHTMLFooter($this->PdfTemplateFooter());
        $mpdf->WriteHTML($html);
        

        $student = $this->__db->execute("SELECT nr_index FROM student WHERE id ='".$idStudent."' LIMIT 1")[0];
        $filename='deklaracja_'.$student['nr_index'].'.pdf';
        if(isset($this->__params[2])){
            if(isset($this->__params[2])=="save"){
                ob_clean(); 
                $mpdf->Output($filename ,'D');
            }
        }
        else{
            ob_clean(); 
            $mpdf->Output($filename, 'I');
        }
        return 1; 
        
    } catch (Exception $e) {
          return 0;
        }

    }
}