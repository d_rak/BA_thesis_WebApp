# Internet application that supports the registration and reservation process of diploma theses - Bachelor's thesis 2019

## ℹ️ README PL
[README w języku polskim](README_PL.md)

## 🖥 Visuals (screen)
<img src="screenshot/praca_licencjacka_portfolio.webp" width="850">

## 📝 Description
The aim of the project was to create an application from scratch that would allow students to book bachelor's and master's thesis topics. I took into account the possibility of adding new topics by promoters through a prepared file template. The application was also supposed to contain an administration panel for secretarial employees.

The presented topic was proposed by me after noticing difficulties by students when booking available topics for bachelor's theses.
 
<br>
<br>

More information about the project is available at the link [itsparta.pl/CV/DariuszRak/#BA_thesis_app](https://itsparta.pl/CV/DariuszRak/#BA_thesis_app)

I invite you to see the page. 🙂 

## 🎯 Function

The main functions have been implemented in the application:
- adding topics of diploma theses based on a prepared .docx file template
- booking or confirmation of assignment of the subject of the diploma thesis by the Student (preliminary booking)
- generating a declaration of choosing the topic of the diploma thesis (PDF file)
- sending an email (confirmation of pre-booking the topic)
- administration panel for the secretary's office (verification and final approval of the choice of the diploma thesis topic)
- statistics of free and busy topics (in the administration panel)
- deleting diploma theses topics (option in the administration panel)

## 💡 Used technologies \ libraries \ tools \ assumption
![PHP](https://img.shields.io/badge/-PHP7-787cb4) 
![JavaScript](https://img.shields.io/badge/-JavaScript-edd619) 
![MariaDB](https://img.shields.io/badge/-MariaDB-b76f54) 
![HTML5](https://img.shields.io/badge/-HTML5-e54e26) 
![CSS3](https://img.shields.io/badge/-CSS3-1472b7) 
![Bootstrap](https://img.shields.io/badge/-Bootstrap4-703cbe) 
![Git](https://img.shields.io/badge/-Git-f05033) 

![mPDF](https://img.shields.io/badge/-mPDF-7d7d7d) 
![PHPMailer](https://img.shields.io/badge/-PHPMailer-7d7d7d) 
![PHPWord](https://img.shields.io/badge/-PHPWord-7d7d7d) 

![WebApp](https://img.shields.io/badge/-WebApp-lightgrey) 
![MVC](https://img.shields.io/badge/-MVC-lightgrey) 

## 📄  Setup \ testing the project

To run this project, install it locally using
```sh
$ cd .\.docker\
$ docker-compose build
$ docker-compose up
```

## 📄 Notes
I know that in this project can be improved a lot in terms of: graphics, UI/UX, security and the code itself. It was an educational project and currently not developed. I publish it to show my initial development path, and the progress in relation to the next built projects in the future.

By the way, dear reader, a little throwback to the past. 🙂 Recall your own research work: bachelor's or master's. Do you also remember it positively (with a tear in your eye) 😉?

## 🙋‍♂️ Contact
Contact on the website [itsparta.pl/CV/DariuszRak/](https://itsparta.pl/CV/DariuszRak/)

## 📄 License
I allow you to download and test the code. However, I would like to point out that the code run / used is at your own risk.

## 🏁 Project status
The project was created for educational purposes in 2019. It is currently not developed.




